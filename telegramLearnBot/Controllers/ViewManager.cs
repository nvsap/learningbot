﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using telegramLearnBot.Models;
using Microsoft.EntityFrameworkCore;
using Telegram.Bot.Types.ReplyMarkups;
using telegramLearnBot.Models.LearningModels;
using Telegram.Bot.Types.Payments;

namespace telegramLearnBot.Controllers
{
    public class ViewManager
    {
        #region Admin View
        public static async void LessonViewAdmin(TelegramBotClient botClient, TelegramContext db, Message message, Guid LessonId)
        {
            //var homeWork = db.HomeWorks.Where(x => x.Lesson.Id == LessonId).Include(y => y.Lesson).Single();
            var lesson = db.Lessons.Where(x => x.Id == LessonId).Single();
            var files = db.DataFiles.Where(x => x.ParentId == LessonId).ToList();

            //await botClient.SendTextMessageAsync(message.Chat.Id, "*" + lesson.Name + "*" + "\n\n" + lesson.Desciption + "\n" + lesson.PostTime, Telegram.Bot.Types.Enums.ParseMode.Markdown);
            foreach(var file in files)
            {
                await botClient.SendDocumentAsync(message.Chat.Id, file.FileId);
            }
            var keyboardInline = new InlineKeyboardButton[4][];
            keyboardInline[0] = new[]
                {
                    new InlineKeyboardButton { Text = "Изменить название", CallbackData = "eln" + lesson.Id },
                    new InlineKeyboardButton { Text = "Изменить описание", CallbackData = "eld" + lesson.Id }
                };
            keyboardInline[1] = new[]
                {
                    new InlineKeyboardButton { Text = "Установить время рассылки", CallbackData = "elt" + lesson.Id },
                };
            keyboardInline[2] = new[]
                {
                    new InlineKeyboardButton { Text = "Удалить все файлы", CallbackData = "dlf" + lesson.Id },
                    new InlineKeyboardButton { Text = "Добавить файлы", CallbackData = "alf" + lesson.Id }
                };
            keyboardInline[3] = new[]
                {
                    new InlineKeyboardButton { Text = "Назад", CallbackData = "L" + lesson.Id }
                };
            var keyboard = new InlineKeyboardMarkup(keyboardInline);
            await botClient.SendTextMessageAsync(message.Chat.Id, "Редактировать урок *" + lesson.Name + "*", Telegram.Bot.Types.Enums.ParseMode.Markdown, false, false, 0, keyboard);
        }
        public static async void PaymentsListInitialize(TelegramBotClient botClient, TelegramContext db, Message message, int pageNum)
        {

            try
            {
                List<Payment> paymentsList;

                var pageCount = Convert.ToInt16(Math.Ceiling((decimal)db.Payments.Count() / 5));
                var lastPageItemsCount = db.Payments.Count() % 5;
                if (lastPageItemsCount == 0 && db.Payments.Count() != 0) lastPageItemsCount = 5;
                bool lastPage;
                if (pageCount <= pageNum + 1)
                {
                    paymentsList = db.Payments.OrderBy(x => x.DateTime).Take(lastPageItemsCount).ToList();
                    lastPage = true;
                }
                else if (pageCount == 1)
                {
                    paymentsList = db.Payments.OrderByDescending(x => x.DateTime).Take(lastPageItemsCount).ToList();
                    lastPage = false;
                }
                else
                {
                    paymentsList = db.Payments.OrderByDescending(x => x.DateTime).Skip(5 * pageNum).Take(5).ToList();
                    lastPage = false;
                }
                
                var pageContent = "<b>Список оплат</b>\n\n";
                foreach (var line in paymentsList)
                {

                    var User = db.Users.Where(z => z.UserId == line.UserId).Single();
                    if (line.WebinarId.HasValue)
                    {
                        var Webinar = db.Webinars.Where(x => x.Id == line.WebinarId).SingleOrDefault();
                        pageContent = pageContent + "Вебинар '" + Webinar.Name + "'.\n Цена: " + Webinar.Amount + " руб.\n Пользователь: " + User.Name + "\n Статус: " + (line.Status ? "Доступ открыт" : "Доступ закрыт") + "\n Меню оплаты: /payment" + line.UnicId + "\n\n";

                    }
                    else
                    {
                        var CourseType = db.CoursesType.Where(x => x.Id == line.CourseTypeId).Include(x => x.Course).Single();
                        var CourseName = db.Courses.Where(y => y.Id == CourseType.Course.Id).Select(x => x.Name).Single();
                        pageContent = pageContent + "Тариф '" + CourseType.Name + "' курса '" + CourseName + "'.\n Цена: " + CourseType.PaymentAmount + "руб. \n Пользователь: " + User.Name + "\n" + line.DateTime +  "\n Статус: " + (line.Status ? "Доступ открыт" : "Доступ закрыт") + "\n Меню оплаты: /payment" + line.UnicId + "\n\n";
                    }
                    //var CourseType = db.CoursesType.Where(x => x.Id == line.CourseTypeId).Include(x => x.Course).Single();
                    //var CourseName = db.Courses.Where(y => y.Id == CourseType.Course.Id).Select(x => x.Name).Single();
                    //pageContent = pageContent + "Тариф '" + CourseType.Name + "' курса '" + CourseName + "'.\n Цена: " + CourseType.PaymentAmount + "\nПользователь: " + User.Name + "\n\nМеню оплаты: /payment" + line.UnicId + " \n Статус: " + (line.Status ? "Доступ открыт\n\n" : "Доступ закрыт\n\n");
                }

                var keyboardInline = new InlineKeyboardButton[2][];
                if (pageCount == 1)
                {
                    keyboardInline[0] = new InlineKeyboardButton[]
                        {
                            new InlineKeyboardButton { Text = "-" + (pageNum + 1) + "-", CallbackData = "PL" + pageNum }
                        };
                }
                else if (pageCount == 2)
                {
                    keyboardInline[0] = new InlineKeyboardButton[]
                        {
                            new InlineKeyboardButton { Text = "-" + (pageNum + 1) + "-", CallbackData = "PL0" },
                            new InlineKeyboardButton { Text = "-" + (pageNum + 2) + "-", CallbackData = "PL1"}
                        };
                }
                else
                {
                    if ((1 - pageNum) == 1)
                    {
                        keyboardInline[0] = new InlineKeyboardButton[]
                        {
                            new InlineKeyboardButton { Text = "-1-", CallbackData = "PL0" },
                            new InlineKeyboardButton { Text = (pageNum + 2) + " >", CallbackData = "PL" + (pageNum + 1)},
                            new InlineKeyboardButton { Text = (pageNum + 3) + " >", CallbackData = "PL" + (pageNum + 2)},
                            new InlineKeyboardButton { Text = (pageNum + 4) + " >", CallbackData = "PL" + (pageNum + 3)},
                            new InlineKeyboardButton { Text = (pageCount) + " >>", CallbackData = "PL" + (pageCount)}
                        };
                    }
                    else if (lastPage)
                    {
                        keyboardInline[0] = new InlineKeyboardButton[]
                        {
                            new InlineKeyboardButton { Text = "<< 1", CallbackData = "PL0" },
                            new InlineKeyboardButton { Text = "< " + (pageNum - 2), CallbackData = "PL" + (pageNum - 3)},
                            new InlineKeyboardButton { Text = "< " + (pageNum - 1), CallbackData = "PL" + (pageNum - 2)},
                            new InlineKeyboardButton { Text = "< " + pageNum, CallbackData = "PL" + (pageNum - 1)},
                            new InlineKeyboardButton { Text = "-" + (pageNum + 1) + "-", CallbackData = "PL" + pageNum}
                        };
                    }
                    else
                    {
                        keyboardInline[0] = new InlineKeyboardButton[]
                       {
                            new InlineKeyboardButton { Text = "<< 1", CallbackData = "PL0" },
                            new InlineKeyboardButton { Text = "< " + (pageNum), CallbackData = "PL" + (pageNum - 1)},
                            new InlineKeyboardButton { Text = "-" + (pageNum + 1) + "-", CallbackData = "PL" + pageNum},
                            new InlineKeyboardButton { Text = (pageNum + 2) + " >", CallbackData = "PL" + (pageNum + 1)},
                            new InlineKeyboardButton { Text = (pageCount) + " >>", CallbackData = "PL" + (pageCount - 1)}
                       };
                        
                    }
                }
                keyboardInline[1] = new InlineKeyboardButton[]
                    {
                        new InlineKeyboardButton { Text = "Назад", CallbackData = "Payment" }
                    };
                var keyboard = new InlineKeyboardMarkup(keyboardInline);

                await botClient.SendTextMessageAsync(message.Chat.Id, pageContent, Telegram.Bot.Types.Enums.ParseMode.Html, false, false, 0, keyboard);
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, $"Ошибка: \n" + ex.Message);
            }
        }
        public static async void PaymentsSearch(TelegramBotClient botClient, TelegramContext db, Message message)
        {
            var User = db.Users.Where(x => x.Name == message.Text).Single();
            var Payments = db.Payments.Where(x => x.UserId == User.UserId).ToList();
            var pageContent = "<b>Список найденых оплат</b>\n\n";
            
            foreach (var line in Payments)
            {
                if (line.WebinarId.HasValue)
                {
                    var Webinar = db.Webinars.Where(x => x.Id == line.WebinarId).SingleOrDefault();
                    pageContent = pageContent + "Вебинар '" + Webinar.Name + "'.\n Цена: " + Webinar.Amount + " руб.\n Пользователь: " + User.Name + "\n Статус: " + (line.Status ? "Доступ открыт" : "Доступ закрыт") + "\n Меню оплаты: /payment" + line.UnicId + "\n\n";

                }
                else
                {
                    var CourseType = db.CoursesType.Where(x => x.Id == line.CourseTypeId).Include(x => x.Course).Single();
                    var CourseName = db.Courses.Where(y => y.Id == CourseType.Course.Id).Select(x => x.Name).Single();
                    pageContent = pageContent + "Тариф '" + CourseType.Name + "' курса '" + CourseName + "'.\n Цена: " + CourseType.PaymentAmount + " руб.\n Пользователь: " + User.Name + "\n Статус: " + (line.Status ? "Доступ открыт" : "Доступ закрыт") + "\n Меню оплаты: /payment" + line.UnicId + "\n\n";
                }
            }
            var keyboard = new InlineKeyboardMarkup(
                    new InlineKeyboardButton { Text = "Назад", CallbackData = "Payment" }
                );
            await botClient.SendTextMessageAsync(message.Chat.Id, pageContent, Telegram.Bot.Types.Enums.ParseMode.Html, false, false, 0, keyboard);
        }
        public static async void PaymentSelect(TelegramBotClient botClient, Message message)
        {
            using (TelegramContext db = new TelegramContext())
            {
                var PaymentId = Convert.ToInt16(message.Text.Remove(0, 8));
                var Payment = db.Payments.Where(x => x.UnicId == PaymentId).Single();
                var User = db.Users.Where(z => z.UserId == Payment.UserId).Single();
                string pageContent = "";
                if (Payment.WebinarId.HasValue)
                {
                    var Webinar = db.Webinars.Where(x => x.Id == Payment.WebinarId).SingleOrDefault();
                    pageContent = "Вебинар '" + Webinar.Name + "'.\n Цена: " + Webinar.Amount  + " руб.\n Пользователь: " + User.Name + "\n Статус: " + (Payment.Status ? "Доступ открыт" : "Доступ закрыт");

                }
                else
                {
                    var CourseType = db.CoursesType.Where(x => x.Id == Payment.CourseTypeId).Include(x => x.Course).Single();
                    var CourseName = db.Courses.Where(y => y.Id == CourseType.Course.Id).Select(x => x.Name).Single();
                    pageContent = "Тариф '" + CourseType.Name + "' курса '" + CourseName + "'.\n Цена: " + CourseType.PaymentAmount + " руб.\n Пользователь: " + User.Name + "\n Статус: " + (Payment.Status ? "Доступ открыт" : "Доступ закрыт");
                }
                var keyboard = new InlineKeyboardMarkup( new[] 
                    {
                        new InlineKeyboardButton { Text = (Payment.Status ? "Закрыть доступ" : "Открыть доступ"), CallbackData = "GA" + Payment.Id},//Get Access
                        new InlineKeyboardButton { Text = "Назад", CallbackData = "Payment" }
                    }
                );
                await botClient.SendTextMessageAsync(message.Chat.Id, pageContent, Telegram.Bot.Types.Enums.ParseMode.Html, false, false, 0, keyboard);
            }
        }

        //public static async void HomeWorkView(TelegramBotClient botClient, TelegramContext db, Message message, Guid HoneworkId)
        //{
        //    var homeWork = db.HomeWorks.Where(x => x.Id == HoneworkId).Include(y => y.Lesson).Single();
        //    var files = db.DataFiles.Where(x => x.ParentId == homeWork.Id).ToList();

        //    await botClient.SendTextMessageAsync(message.Chat.Id, "*ДЗ " + homeWork.Lesson.Name + "*" + "\n\n" + homeWork.Description, Telegram.Bot.Types.Enums.ParseMode.Markdown);
        //    foreach (var file in files)
        //    {
        //        await botClient.SendDocumentAsync(message.Chat.Id, file.FileId);
        //    }
        //    var keyboardInline = new InlineKeyboardButton[2][];
        //    keyboardInline[0] = new[]
        //        {
        //            new InlineKeyboardButton { Text = "Редактировать описание ДЗ.", CallbackData = "ehd" + homeWork.Id }
        //        };
        //    keyboardInline[1] = new[]
        //        {
        //            new InlineKeyboardButton { Text = "Удалить все файлы ДЗ.", CallbackData = "dhf" + homeWork.Id },
        //            new InlineKeyboardButton { Text = "Добавить файлы к ДЗ.", CallbackData = "ahf" + homeWork.Id },
        //        };
        //    var keyboard = new InlineKeyboardMarkup(keyboardInline);
        //    await botClient.SendTextMessageAsync(message.Chat.Id, "Редактировать ДЗ *" + homeWork.Lesson.Name + "*", Telegram.Bot.Types.Enums.ParseMode.Markdown, false, false, 0, keyboard);
        //}
        #endregion

        #region UserView
        #region WebinarReg
        public static async void WebinarRegister(TelegramBotClient botClient, TelegramContext db, Message message, Webinars webinar)
        {

            Message mess;
            try
            {
                if (webinar != null)
                {
                    //StateController.AddProgress
                    //var webinarUser = db.WebinarsUsers.Where(x => x.User.UserId == message.Chat.Id && x.Webinars.Id == webinar.Id).SingleOrDefault();
                    var userPayment = db.Payments.Where(x => x.UserId == message.Chat.Id && x.WebinarId == webinar.Id).SingleOrDefault();

                    var keyboardInline = new InlineKeyboardButton[1][];
                    var messageToUser = $"*" + webinar.Name + "*\n\n Цена: " + webinar.Amount + " руб.\n\n" + webinar.Description + "\n\n Дата: " + webinar.WebinarDate.ToString("dd/MM/yyyy HH:mm");
                    if (userPayment != null && userPayment.Status)
                    {
                        var arrays = new List<InlineKeyboardButton>();
                        arrays.Add(new InlineKeyboardButton { Text = "Назад", CallbackData = "MainMenu" });
                        if (!String.IsNullOrEmpty(webinar.OnlineLink))
                        {
                            arrays.Add(new InlineKeyboardButton { Text = "Ссылка на трансляцию", Url = webinar.OnlineLink });
                            //messageToUser += "\n\n Ссылка на трансляцию <" + webinar.OnlineLink + ">";
                        }
                        else if (!String.IsNullOrEmpty(webinar.SavedTranslationLink))
                        {
                            arrays.Add(new InlineKeyboardButton { Text = "Ссылка на запись", Url = webinar.SavedTranslationLink });
                            //messageToUser += "\n\n Ссылка на запись <" + webinar.SavedTranslationLink + ">";
                        }
                        keyboardInline[0] = arrays.ToArray();
                    }
                    else
                    {
                        keyboardInline[0] = new[]
                            {
                            new InlineKeyboardButton { Text = "Запрос на покупку", CallbackData = "PRW" + webinar.Id },
                            new InlineKeyboardButton { Text = "Назад", CallbackData = "MainMenu"},
                            };
                                            }
                    var keyboard = new InlineKeyboardMarkup(keyboardInline);
                    mess = await botClient.SendTextMessageAsync(message.Chat.Id, messageToUser, Telegram.Bot.Types.Enums.ParseMode.Markdown, false, false, 0, keyboard);

                }
                else
                {
                    var keyboardInline = new InlineKeyboardButton[1][];
                    keyboardInline[0] = new[]
                        {
                        new InlineKeyboardButton { Text = "Назад", CallbackData = "MainMenu"},
                    };
                    var keyboard = new InlineKeyboardMarkup(keyboardInline);
                    mess = await botClient.SendTextMessageAsync(message.Chat.Id, "Активных вебинаров нет", Telegram.Bot.Types.Enums.ParseMode.Default, false, false, 0, keyboard);
                }

                StateController.SetLastBotMessageId(message.Chat.Id, mess.MessageId);
            }
            catch (Exception ex)
            {
                mess = await botClient.SendTextMessageAsync(message.Chat.Id, $"Ошибка: \n" + ex.Message);
                StateController.SetLastBotMessageId(message.Chat.Id, mess.MessageId);
            }
            //LabeledPrice price = new LabeledPrice("Default", 7000);
            //List<LabeledPrice> prices = new List<LabeledPrice>();
            //prices.Add(price);
            //IEnumerable<Telegram.Bot.Types.Payments.LabeledPrice> pricesIEnum = prices.AsEnumerable<LabeledPrice>();
            //await botClient.SendInvoiceAsync((int)message.Chat.Id, "Payment", "TestPayment", "Classic", "632593626:TEST:i56982357197", "PayClassic", "RUB", pricesIEnum);
        }
        #endregion
        #region NewCourses
        public static async void NewCoursesList(TelegramBotClient botClient, TelegramContext db, Message message)
        {
            var mess = new Message();
            try
            {
                var userProgres = db.Progresses.Where(x => x.User == message.Chat.Id && x.ProgressObjectType == (int)ProgressObject.Course).ToList();
                List<Course> NewCourses = new List<Course>();
                NewCourses = db.Courses.Where(x => !x.IsHiden).ToList();
                if (userProgres != null)
                {
                    List<Course> courseList = new List<Course>();
                    foreach (var cour in userProgres)
                    {
                        courseList.Add(db.Courses.Where(x => x.Id == cour.ProgressObjectId).Single());
                    }
                    var res = NewCourses.Except(courseList);
                    NewCourses = res.ToList<Course>();
                }
                if (NewCourses.Count > 0)
                {
                    var keyboardInline = new InlineKeyboardButton[NewCourses.Count + 1][];
                    int i = 0;
                    foreach (var course in NewCourses)
                    {
                        var name = course.Name == null ? "*empty course name*" : course.Name;
                        keyboardInline[i] = new[]
                        {
                            new InlineKeyboardButton { Text = name, CallbackData = "NC" + course.Id }
                        };

                        i++;
                    }
                    keyboardInline[i] = new[]
                        {
                            new InlineKeyboardButton { Text = "Назад", CallbackData = "MainMenu" }
                        };
                    var keyboard = new InlineKeyboardMarkup(keyboardInline);
                    mess = await botClient.SendTextMessageAsync(message.Chat.Id, "Список новых курсов", Telegram.Bot.Types.Enums.ParseMode.Default, false, false, 0, keyboard);
                }
                else
                {
                    var keyboardInline = new InlineKeyboardButton[NewCourses.Count + 1][];
                    keyboardInline[0] = new[]
                    {
                        new InlineKeyboardButton { Text = "Назад", CallbackData = "MainMenu" }
                    };
                    var keyboard = new InlineKeyboardMarkup(keyboardInline);
                    mess = await botClient.SendTextMessageAsync(message.Chat.Id, "Курсов нет(", Telegram.Bot.Types.Enums.ParseMode.Default, false, false, 0, keyboard);
                }
                StateController.SetLastBotMessageId(message.Chat.Id, mess.MessageId);
            }
            catch (Exception ex)
            {
                mess = await botClient.SendTextMessageAsync(message.Chat.Id, $"Ошибка: \n" + ex.Message);
                StateController.SetLastBotMessageId(message.Chat.Id, mess.MessageId);
            }

        }
        public static async void SelectedNewCourse(TelegramBotClient botClient, TelegramContext db, Message message, Guid CourseId)
        {
            var mess = new Message();
            try
            {
                var Course = db.Courses.Where(x => x.Id == CourseId).Single();
                if (Course != null)
                {
                    var keyboardInline = new InlineKeyboardButton[2][];
                    var name = Course.Name == null ? "*empty course name*" : Course.Name;
                    keyboardInline[0] = new[]
                    {
                        new InlineKeyboardButton { Text = "Начать обучение", CallbackData = "SC" + CourseId }
                    };
                    keyboardInline[1] = new[]
                    {
                        new InlineKeyboardButton { Text = "Назад", CallbackData = "NewCoursesList"}
                    };
                    var keyboard = new InlineKeyboardMarkup(keyboardInline);
                    mess = await botClient.SendTextMessageAsync(message.Chat.Id, "*" + name + "*" + "\n\n" + Course.Description, Telegram.Bot.Types.Enums.ParseMode.Markdown, false, false, 0, keyboard);
                    StateController.SetLastBotMessageId(message.Chat.Id, mess.MessageId);
                }
                else
                {
                    await botClient.SendTextMessageAsync(message.Chat.Id, "Курса нет", Telegram.Bot.Types.Enums.ParseMode.Default);
                }
            }
            catch (Exception ex)
            {
                mess = await botClient.SendTextMessageAsync(message.Chat.Id, $"Ошибка: \n" + ex.Message);
                StateController.SetLastBotMessageId(message.Chat.Id, mess.MessageId);
            }
        }

        public static async void StartNewCourse(TelegramBotClient botClient, TelegramContext db, Message message, Guid CourseId)
        {
            try
            {
                var CourseTypes = db.CoursesType.Where(x => x.Course.Id == CourseId).Include(x => x.Course).ToList();
                CourseType CourseType = new CourseType();
                CourseType = CourseTypes.Where(x => x.IsDemo).SingleOrDefault();
                if(CourseType == null)
                {
                    CourseType = CourseTypes.Take(1).SingleOrDefault();
                    //var LastPayment = db.Payments.OrderByDescending(x => x.UnicId).Take(1).SingleOrDefault();
                    //int UnicId;
                    //if (LastPayment == null)
                    //{
                    //    UnicId = 1;
                    //}
                    //else
                    //{
                    //    UnicId = LastPayment.UnicId + 1;
                    //}
                    //var Payment = new Payment()
                    //{
                    //    CourseTypeId = CourseType.Id,
                    //    Amount = CourseType.PaymentAmount,
                    //    Id = Guid.NewGuid(),
                    //    IsOffline = true,
                    //    Status = false,
                    //    UserId = (int)message.Chat.Id,
                    //    UnicId = UnicId
                    //};//???
                    //db.Payments.Add(Payment);
                    //db.SaveChanges();
                }
                var Lesson = db.Lessons.Where(x => x.CourseType.Id == CourseType.Id).OrderBy(x => x.SeqNumber).Take(1).Single();
                StateController.AddProgress(CourseId, (int)ProgressObject.Course, (int)message.Chat.Id);
                StateController.AddProgress(CourseType.Id, (int)ProgressObject.Tarif, (int)message.Chat.Id);
                StateController.AddProgress(Lesson.Id, (int)ProgressObject.Lesson, (int)message.Chat.Id);
                MyTarifSelect(botClient, db, message, CourseType.Id);
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, $"Ошибка: \n" + ex.Message);
            }
        }

        #endregion
        #region MyCourses
        public static async void MyCoursesList(TelegramBotClient botClient, TelegramContext db, Message message)
        {
            var mess = new Message();
            try
            {
                var userProgres = db.Progresses.Where(x => x.User == message.Chat.Id && x.ProgressObjectType == (int)ProgressObject.Course).ToList();
                List<Course> Courses = new List<Course>();
                if (userProgres != null)
                {
                    foreach (var cour in userProgres)
                    {
                        Courses.Add(db.Courses.Where(x => x.Id == cour.ProgressObjectId).Single());
                    }
                }
                if (Courses.Count > 0)
                {
                    var keyboardInline = new InlineKeyboardButton[Courses.Count + 1][];
                    int i = 0;
                    foreach (var course in Courses)
                    {
                        var name = course.Name == null ? "*empty course name*" : course.Name;
                        keyboardInline[i] = new[]
                        {
                            new InlineKeyboardButton { Text = name, CallbackData = "MC" + course.Id }
                        };
                        i++;
                    }
                    keyboardInline[i] = new[]
                        {
                            new InlineKeyboardButton { Text = "Назад", CallbackData = "MainMenu"}
                        };
                    var keyboard = new InlineKeyboardMarkup(keyboardInline);
                    mess = await botClient.SendTextMessageAsync(message.Chat.Id, "Список твоих курсов", Telegram.Bot.Types.Enums.ParseMode.Default, false, false, 0, keyboard);
                }
                else
                {
                    var keyboardInline = new InlineKeyboardButton[1][];
                    keyboardInline[0] = new[]
                    {
                        new InlineKeyboardButton { Text = "Назад", CallbackData = "MainMenu" }
                    };
                    var keyboard = new InlineKeyboardMarkup(keyboardInline);
                    mess = await botClient.SendTextMessageAsync(message.Chat.Id, "😢У тебя еще нет курсов. Но ты можешь выбрать подходящий в главном меню", Telegram.Bot.Types.Enums.ParseMode.Default, false, false, 0, keyboard);
                }
                StateController.SetLastBotMessageId(message.Chat.Id, mess.MessageId);
            }
            catch (Exception ex)
            {
                mess = await botClient.SendTextMessageAsync(message.Chat.Id, $"Ошибка: \n" + ex.Message);
                StateController.SetLastBotMessageId(message.Chat.Id, mess.MessageId);
            }
        }
        public static async void MyCourseSelect(TelegramBotClient botClient, TelegramContext db, Message message, Guid CourseId)
        {
            var mess = new Message();
            try
            {
                var userProgres = from prog in db.Progresses
                               join courseType in db.CoursesType on prog.ProgressObjectId equals courseType.Id
                               where prog.User == message.Chat.Id
                               && courseType.Course.Id == CourseId
                               select prog;
                //var userProgres = db.Progresses.Where(x => x.User == message.Chat.Id && x.ProgressObjectType == (int)ProgressObject.Tarif).ToList();
                var CourseType = db.CoursesType.Where(x => x.Course.Id == CourseId).Include(x => x.Course).ToList();
                List<CourseType> CourseTypes = new List<CourseType>();
                List<CourseType> CourseTypeInUseList = new List<CourseType>();
                var keyboardInline = new InlineKeyboardButton[CourseType.Count + 1][];
                int i = 0;
                if (userProgres != null)
                {
                    foreach (var cour in userProgres)
                    {
                        var tarif = db.CoursesType.Where(x => x.Id == cour.ProgressObjectId).Single();
                        CourseTypeInUseList.Add(tarif);
                        var name = "";
                        if (cour.IsFinished)
                        {
                            name = tarif.Name == null ? "*empty tarif name*" : tarif.Name + ". Закончен.";
                        }
                        else
                        {
                            name = tarif.Name == null ? "*empty tarif name*" : tarif.Name + ". В прогрессе.";
                        }
                        keyboardInline[i] = new[]
                        {
                            new InlineKeyboardButton { Text = name, CallbackData = "MT" + tarif.Id }
                        };
                        i++;
                    }
                    var res = CourseType.Except(CourseTypeInUseList);
                    CourseTypes = res.ToList<CourseType>();
                }
                else
                {
                    CourseTypes = CourseType;
                }
                if (CourseTypes.Count > 0)
                {
                    foreach (var tarif in CourseTypes)
                    {
                        var name = tarif.Name == null ? "*empty tarif name*" : tarif.Name + ". Новый";
                        keyboardInline[i] = new[]
                        {
                            new InlineKeyboardButton { Text = name, CallbackData = "MN" + tarif.Id }
                        };
                        i++;
                    }
                }
                keyboardInline[i] = new[]
                        {
                            new InlineKeyboardButton { Text = "Назад", CallbackData = "MyCoursesList"}
                        };
                var keyboard = new InlineKeyboardMarkup(keyboardInline);
                mess = await botClient.SendTextMessageAsync(message.Chat.Id, "Список тарифов курса", Telegram.Bot.Types.Enums.ParseMode.Default, false, false, 0, keyboard);
                StateController.SetLastBotMessageId(message.Chat.Id, mess.MessageId);
            }
            catch (Exception ex)
            {
                mess = await botClient.SendTextMessageAsync(message.Chat.Id, $"Ошибка: \n" + ex.Message);
                StateController.SetLastBotMessageId(message.Chat.Id, mess.MessageId);
            }
        }
        public static async void MyTarifSelect(TelegramBotClient botClient, TelegramContext db, Message message, Guid CourseTypeId)
        {
            var mess = new Message();
            try
            {
                var courseType = db.CoursesType.Where(x => x.Id == CourseTypeId).Include(x => x.Course).Single();
                bool access = true;
                if (!courseType.IsDemo)
                {
                    var userPayment = db.Payments.Where(x => x.UserId == message.Chat.Id && x.CourseTypeId == CourseTypeId).SingleOrDefault();
                    if(userPayment != null)
                    {
                        access = userPayment.Status;
                    }
                    else
                    {
                        access = false;
                    }
                }
                if (access)
                {
                    var userProgres = from prog in db.Progresses
                                      join lesson in db.Lessons on prog.ProgressObjectId equals lesson.Id
                                      where prog.User == message.Chat.Id
                                      && lesson.CourseType.Id == CourseTypeId
                                      select prog;
                    //var userProgres = db.Progresses.Where(x => x.User == message.Chat.Id && x.ProgressObjectType == (int)ProgressObject.Lesson).ToList();
                    var allLessons = db.Lessons.Where(x => x.CourseType.Id == CourseTypeId && x.IsPublished).ToList();
                    List<Lesson> lessons = new List<Lesson>();
                    int i = 0;
                    foreach (var les in userProgres)
                    {
                        var l = allLessons.Where(x => x.Id == les.ProgressObjectId).SingleOrDefault();
                        if (l != null)
                        {
                            lessons.Add(l);
                        }
                    }
                    var keyboardInline = new InlineKeyboardButton[lessons.Count + 1][];
                    foreach (var les in lessons.OrderBy(x=> x.SeqNumber))
                    {
                        var name = les.Name == null ? "*empty lesson name*" : les.Name;
                        keyboardInline[i] = new[]
                        {
                            new InlineKeyboardButton { Text = name, CallbackData = "ML" + les.Id }
                        };
                        i++;
                    }
                    keyboardInline[i] = new[]
                            {
                            new InlineKeyboardButton { Text = "Назад", CallbackData = "MC" + courseType.Course.Id}
                        };
                    var keyboard = new InlineKeyboardMarkup(keyboardInline);
                    mess = await botClient.SendTextMessageAsync(message.Chat.Id, "Список уроков ", Telegram.Bot.Types.Enums.ParseMode.Default, false, false, 0, keyboard);
                    StateController.SetLastBotMessageId(message.Chat.Id, mess.MessageId);
                }
                else
                {
                    MyNewTarifStart(botClient, db, message, CourseTypeId);
                }
            }
            catch (Exception ex)
            {
                mess = await botClient.SendTextMessageAsync(message.Chat.Id, $"Ошибка: \n" + ex.Message);
                StateController.SetLastBotMessageId(message.Chat.Id, mess.MessageId);
            }
        }
        public static async void MyNewTarifStart(TelegramBotClient botClient, TelegramContext db, Message message, Guid CourseTypeId) // Меню действий при запуске доступного, но не купленого курса
        {
            var mess = new Message();
            try
            {
                //StateController.AddProgress
                var CourseType = db.CoursesType.Where(x => x.Id == CourseTypeId).Include(y => y.Course).Single();
                var keyboardInline = new InlineKeyboardButton[1][];
                keyboardInline[0] = new[]
                    {
                        new InlineKeyboardButton { Text = "Запрос на покупку", CallbackData = "PR" + CourseTypeId },
                        new InlineKeyboardButton { Text = "Назад", CallbackData = "MC" + CourseType.Course.Id },
                    };
                var keyboard = new InlineKeyboardMarkup(keyboardInline);
                mess = await botClient.SendTextMessageAsync(message.Chat.Id, "*" + CourseType.Name + "*\n\n" + CourseType.Description + "\n\n _Цена тарифа: " + CourseType.PaymentAmount + " руб._", Telegram.Bot.Types.Enums.ParseMode.Markdown, false, false, 0, keyboard);
                StateController.SetLastBotMessageId(message.Chat.Id, mess.MessageId);
            }
            catch (Exception ex)
            {
                mess = await botClient.SendTextMessageAsync(message.Chat.Id, $"Ошибка: \n" + ex.Message);
                StateController.SetLastBotMessageId(message.Chat.Id, mess.MessageId);
            }
        }
        public static async void LessonView(TelegramBotClient botClient, TelegramContext db, Message message, Guid LessonId)
        {
            var mess = new Message();
            try
            {
                var lesson = db.Lessons.Where(x => x.Id == LessonId).Include(x => x.CourseType).Single();
                var files = db.DataFiles.Where(x => x.ParentId == LessonId).ToList();
                var progress = db.Progresses.Where(x => x.ProgressObjectId == LessonId && x.User == message.Chat.Id).SingleOrDefault();

                StateController.UpdateProgressToFinish(lesson.Id, (int)message.Chat.Id);

                var isLastLesson = db.Lessons.Max(x => x.SeqNumber) == lesson.SeqNumber;
                var isNextLessonPublished = false;
                if (!isLastLesson)
                {
                    var NextLesson = db.Lessons.Where(x => x.CourseType == lesson.CourseType && x.SeqNumber == lesson.SeqNumber + 1).Single();
                    var progressNext = db.Progresses.Where(x => x.ProgressObjectId == NextLesson.Id && x.User == message.Chat.Id).SingleOrDefault();
                    if (progressNext == null)
                        StateController.AddProgress(NextLesson.Id, (int)ProgressObject.Lesson, (int)message.Chat.Id);
                    isNextLessonPublished = NextLesson.IsPublished;
                }
                else
                    StateController.UpdateProgressToFinish(lesson.CourseType.Id, (int)message.Chat.Id);

                var keyboardInline = new InlineKeyboardButton[1][];
                if (!isNextLessonPublished)
                {
                    
                    keyboardInline[0] = new[]
                    {
                        new InlineKeyboardButton { Text = "Назад", CallbackData = "MT" + lesson.CourseType.Id }
                    };
                }
                else
                {
                    
                    keyboardInline[0] = new[]
                    {
                        new InlineKeyboardButton { Text = "Следующий урок", CallbackData = "NL" + lesson.Id },
                        new InlineKeyboardButton { Text = "Назад", CallbackData = "MT" + lesson.CourseType.Id }
                    };
                }
                var keyboard = new InlineKeyboardMarkup(keyboardInline);
                mess = await botClient.SendTextMessageAsync(message.Chat.Id, "*" + lesson.Name + "*" + "\n\n" + lesson.Desciption, Telegram.Bot.Types.Enums.ParseMode.Markdown, false, false, 0, keyboard);
                StateController.SetLastBotMessageId(message.Chat.Id, mess.MessageId);
                foreach (var file in files)
                {
                    await botClient.SendDocumentAsync(message.Chat.Id, file.FileId);
                }

            }
            catch (Exception ex)
            {
                mess = await botClient.SendTextMessageAsync(message.Chat.Id, $"Ошибка: \n" + ex.Message);
                StateController.SetLastBotMessageId(message.Chat.Id, mess.MessageId);
            }
        }
        #endregion
        #endregion
    }
}
