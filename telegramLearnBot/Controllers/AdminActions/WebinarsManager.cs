﻿using System;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using telegramLearnBot.Models;
using telegramLearnBot.Models.LearningModels;
using Telegram.Bot.Types.ReplyMarkups;

namespace telegramLearnBot.Controllers.AdminActions
{
    public class WebinarsManager
    {
        #region Webinar Add
        public static async void CreateWebinars(TelegramBotClient botClient, TelegramContext db, Message message)
        {
            await botClient.SendTextMessageAsync(message.Chat.Id, "Введите название вебинара." + "\n\n Что бы закончить создание курса введите команду /exit", Telegram.Bot.Types.Enums.ParseMode.Markdown);
            StateController.StateUpdate(message.Chat.Id, (int)AdminState.AddWebinarName, true);//AddCourseName
        }
        public static async void AddWebinarName(TelegramBotClient botClient, TelegramContext db, Message message)
        {
            try
            {
                var webinars = db.Webinars.Where(x => x.Name == message.Text).ToList();
                if (webinars.Count > 0)
                {
                    await botClient.SendTextMessageAsync(message.Chat.Id, "Такой вебинар уже есть, введите другое название!", Telegram.Bot.Types.Enums.ParseMode.Default);
                }
                else
                {
                    var Webinar = new Webinars() { Name = message.Text };
                    db.Webinars.Add(Webinar);
                    StateController.StateUpdate(message.From.Id, (int)AdminState.AddWebinarDescription, true, Webinar.Id);
                    db.SaveChanges();
                    await botClient.SendTextMessageAsync(message.Chat.Id, "Введите описание вебинара", Telegram.Bot.Types.Enums.ParseMode.Default);
                }
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, "Ошибка: " + ex.Message, Telegram.Bot.Types.Enums.ParseMode.Default);
            }
        }

        public static async void AddWebinarDescription(TelegramBotClient botClient, TelegramContext db, Message message, Guid CourseId)
        {
            try
            {
                var webinar = db.Webinars.Where(x => x.Id == CourseId).ToList();//This is webinar id
                webinar[0].Description = message.Text;
                db.Webinars.Update(webinar[0]);
                StateController.StateUpdate(message.From.Id, (int)AdminState.AddWebinarDate, true, CourseId);
                db.SaveChanges();
                await botClient.SendTextMessageAsync(message.Chat.Id, "Введите дату и время вебинара в фомате '2009-05-08 14:40'", Telegram.Bot.Types.Enums.ParseMode.Default);
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, "Ошибка: " + ex.Message, Telegram.Bot.Types.Enums.ParseMode.Default);
            }
        }
        public static async void AddWebinarDate(TelegramBotClient botClient, TelegramContext db, Message message, Guid CourseId)
        {
            try
            {
                var webinar = db.Webinars.Where(x => x.Id == CourseId).ToList();//This is webinar id
                webinar[0].WebinarDate = DateTime.ParseExact(message.Text, "yyyy-MM-dd HH:mm",
                                       System.Globalization.CultureInfo.InvariantCulture);
                db.Webinars.Update(webinar[0]);
                StateController.StateUpdate(message.From.Id, (int)AdminState.AddWebinarPrice, true, CourseId);
                db.SaveChanges();
                await botClient.SendTextMessageAsync(message.Chat.Id, "Введите цену вебинара.", Telegram.Bot.Types.Enums.ParseMode.Default);

            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, "Ошибка: " + ex.Message, Telegram.Bot.Types.Enums.ParseMode.Default);
            }
        }
        public static async void AddWebinarPrice(TelegramBotClient botClient, TelegramContext db, Message message, Guid CourseId)
        {
            try
            {
                var webinar = db.Webinars.Where(x => x.Id == CourseId).ToList();//This is webinar id
                webinar[0].Amount = Decimal.Parse(message.Text);
                db.Webinars.Update(webinar[0]);
                StateController.StateUpdate(message.From.Id, (int)AdminState.AddWebinarPrice, true, CourseId);
                db.SaveChanges();
                await botClient.SendTextMessageAsync(message.Chat.Id, "Всё агонь. Добавь ссылку на трансляцию до того как начнётся вебинар, и не забудь загрузить ссылку на запись.", Telegram.Bot.Types.Enums.ParseMode.Default);

            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, "Ошибка: " + ex.Message, Telegram.Bot.Types.Enums.ParseMode.Default);
            }
        }
        #endregion
        #region Webinar Edit
        public static async void EditMenuWebinar(TelegramBotClient botClient, TelegramContext db, Message message, Webinars webinar)
        {
            try
            {
                //StateController.StateUpdate(message.Chat.Id, (int)AdminState.AddTarifName, true, course.Id);
                var keyboardInline = new InlineKeyboardButton[7][];
                keyboardInline[0] = new[]
                {
                    new InlineKeyboardButton { Text = "Изменить название", CallbackData = "ewn" + webinar.Id }
                };
                keyboardInline[1] = new[]
                {
                    new InlineKeyboardButton { Text = "Изменить описание", CallbackData = "ewd" + webinar.Id }
                };
                keyboardInline[2] = new[]
                {
                    new InlineKeyboardButton { Text = "Изменить дату", CallbackData = "ewt" + webinar.Id }
                };
                keyboardInline[3] = new[]
                    {
                    new InlineKeyboardButton { Text = "Изменить цену", CallbackData = "ewp" + webinar.Id }
                };
                keyboardInline[4] = new[]
                    {
                    new InlineKeyboardButton { Text = "Изменить ссылку на трансляцию", CallbackData = "ewo" + webinar.Id }
                };
                keyboardInline[5] = new[]
                    {
                    new InlineKeyboardButton { Text = "Изменить ссылку на запись", CallbackData = "ews" + webinar.Id }
                };
                keyboardInline[6] = new[]
                {
                    new InlineKeyboardButton { Text = "Назад", CallbackData = "WebinarsList" }
                };
                var keyboard = new InlineKeyboardMarkup(keyboardInline);
                await botClient.SendTextMessageAsync(message.Chat.Id, "Редактировать вебинар *" + webinar.Name + "*\n\n Цена " + webinar.Amount + "\n\n Описание" + webinar.Description + "\n\n Дата" + webinar.WebinarDate, Telegram.Bot.Types.Enums.ParseMode.Markdown, false, false, 0, keyboard);
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, $"Ошибка: \n" + ex.Message);
            }
        }
        public static async void WebinarsEditName(TelegramBotClient botClient, TelegramContext db, Message message, Guid WebinarId)
        {
            var webinar = db.Webinars.Where(x => x.Id == WebinarId).Single();
            webinar.Name = message.Text;
            db.Webinars.Update(webinar);
            db.SaveChanges();
            EditMenuWebinar(botClient, db, message, webinar);
        }
        public static async void WebinarsEditDescr(TelegramBotClient botClient, TelegramContext db, Message message, Guid WebinarId)
        {
            var webinar = db.Webinars.Where(x => x.Id == WebinarId).Single();
            webinar.Description = message.Text;
            db.Webinars.Update(webinar);
            db.SaveChanges();
            EditMenuWebinar(botClient, db, message, webinar);
        }
        public static async void WebinarsEditPrice(TelegramBotClient botClient, TelegramContext db, Message message, Guid WebinarId)
        {
            var webinar = db.Webinars.Where(x => x.Id == WebinarId).Single();
            try
            {
                webinar.Amount = Decimal.Parse(message.Text);
                db.Webinars.Update(webinar);
                db.SaveChanges();
                EditMenuWebinar(botClient, db, message, webinar);
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, "Ошибка ввода! " + ex.Message);
            }
        }
        public static async void WebinarsListDate(TelegramBotClient botClient, TelegramContext db, Message message, Guid WebinarId)
        {
            var webinar = db.Webinars.Where(x => x.Id == WebinarId).Single();
            try
            {
                webinar.WebinarDate = DateTime.ParseExact(message.Text, "yyyy-MM-dd HH:mm",
                                       System.Globalization.CultureInfo.InvariantCulture);
                db.Webinars.Update(webinar);
                db.SaveChanges();
                EditMenuWebinar(botClient, db, message, webinar);
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, "Ошибка ввода! " + ex.Message);
            }
        }
        public static async void WebinarsListOnlineLink(TelegramBotClient botClient, TelegramContext db, Message message, Guid WebinarId)
        {
            var webinar = db.Webinars.Where(x => x.Id == WebinarId).Single();
            webinar.OnlineLink = message.Text;
            db.Webinars.Update(webinar);
            db.SaveChanges();
            EditMenuWebinar(botClient, db, message, webinar);
        }
        public static async void WebinarsListSavedLink(TelegramBotClient botClient, TelegramContext db, Message message, Guid WebinarId)
        {
            var webinar = db.Webinars.Where(x => x.Id == WebinarId).Single();
            webinar.SavedTranslationLink = message.Text;
            db.Webinars.Update(webinar);
            db.SaveChanges();
            EditMenuWebinar(botClient, db, message, webinar);
        }

        #endregion
        #region WebinarsView
        public static async void WebinarsListShow(TelegramBotClient botClient, TelegramContext db, Message message)
        {
            try
            {
                var webinars = db.Webinars.ToList();
                if (webinars.Count > 0)
                {
                    var keyboardInline = new InlineKeyboardButton[webinars.Count][];
                    int i = 0;
                    foreach (var webinar in webinars)
                    {
                        var name = webinar.Name == null ? "*empty webinar name*" : webinar.Name;
                        keyboardInline[i] = new[]
                        {
                        new InlineKeyboardButton { Text = name, CallbackData = "W" + webinar.Id }
                    };
                        i++;
                    }
                    var keyboard = new InlineKeyboardMarkup(keyboardInline);
                    await botClient.SendTextMessageAsync(message.Chat.Id, "Список вебинаров", Telegram.Bot.Types.Enums.ParseMode.Default, false, false, 0, keyboard);
                }
                else
                {
                    await botClient.SendTextMessageAsync(message.Chat.Id, "Вебинаров нет(", Telegram.Bot.Types.Enums.ParseMode.Default);
                }

            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, $"Ошибка: \n" + ex.Message);
            }

        }
        #endregion
    }
}
    