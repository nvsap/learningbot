﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using telegramLearnBot.Models;
using telegramLearnBot.Models.LearningModels;

namespace telegramLearnBot.Controllers.AdminActions
{
    public class EditManager
    {
        #region Edit
        public static void EditCourseName(TelegramBotClient botClient, TelegramContext db, Message message, Guid CourseId)
        {
            var course = db.Courses.Where(x => x.Id == CourseId).Single();
            course.Name = message.Text;
            db.Courses.Update(course);
            db.SaveChanges();
            //await botClient.SendTextMessageAsync(message.Chat.Id, "Имя курса изменено!");
            CourseManager.TarifListShow(botClient, db, message, course);
        }
        public static void EditCourseDescription(TelegramBotClient botClient, TelegramContext db, Message message, Guid CourseId)
        {
            var course = db.Courses.Where(x => x.Id == CourseId).Single();
            course.Description = message.Text;
            db.Courses.Update(course);
            db.SaveChanges();
            //await botClient.SendTextMessageAsync(message.Chat.Id, "Описание курса изменено!");
            CourseManager.TarifListShow(botClient, db, message, course);
        }
        public static void EditTarifName(TelegramBotClient botClient, TelegramContext db, Message message, Guid CourseTypeId)
        {
            var courseType = db.CoursesType.Where(x => x.Id == CourseTypeId).Include(x => x.Course).Single();
            courseType.Name = message.Text;
            db.CoursesType.Update(courseType);
            db.SaveChanges();
            CourseManager.LessonsMainListShow(botClient, db, message, courseType);
            //await botClient.SendTextMessageAsync(message.Chat.Id, "Название тарифа изменено!");
        }
        public static void EditTarifDescription(TelegramBotClient botClient, TelegramContext db, Message message, Guid CourseTypeId)
        {
            var courseType = db.CoursesType.Where(x => x.Id == CourseTypeId).Include(x => x.Course).Single();
            courseType.Description = message.Text;
            db.CoursesType.Update(courseType);
            db.SaveChanges();
            //await botClient.SendTextMessageAsync(message.Chat.Id, "Название тарифа изменено!");
            CourseManager.LessonsMainListShow(botClient, db, message, courseType);
        }
        public static async void EditTarifPrice(TelegramBotClient botClient, TelegramContext db, Message message, Guid CourseTypeId)
        {
            var courseType = db.CoursesType.Where(x => x.Id == CourseTypeId).Include(x => x.Course).Single();
            try
            {
                courseType.PaymentAmount = Decimal.Parse(message.Text);
            }
            catch(Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, "Ошибка ввода! " + ex.Message);
            }
            db.CoursesType.Update(courseType);
            db.SaveChanges();
            //await botClient.SendTextMessageAsync(message.Chat.Id, "Название тарифа изменено!");
            CourseManager.LessonsMainListShow(botClient, db, message, courseType);
        }
        public static void EditLessonName(TelegramBotClient botClient, TelegramContext db, Message message, Guid LessonId)
        {
            var lesson = db.Lessons.Where(x => x.Id == LessonId).Single();
            lesson.Name = message.Text;
            db.Lessons.Update(lesson);
            db.SaveChanges();
            ViewManager.LessonViewAdmin(botClient, db, message, LessonId);
        }
        public static void EditLessonDesc(TelegramBotClient botClient, TelegramContext db, Message message, Guid LessonId)
        {
            var lesson = db.Lessons.Where(x => x.Id == LessonId).Single();
            lesson.Desciption = message.Text;
            db.Lessons.Update(lesson);
            db.SaveChanges();
            ViewManager.LessonViewAdmin(botClient, db, message, LessonId);
        }
        public static async void EditPostTime(TelegramBotClient botClient, TelegramContext db, Message message, Guid LessonId)
        {
            var lesson = db.Lessons.Where(x => x.Id == LessonId).Single();
            try
            {
                lesson.PostTime = DateTime.ParseExact(message.Text, "yyyy-MM-dd HH:mm",
                                       System.Globalization.CultureInfo.InvariantCulture);
                lesson.IsPublished = false;
                db.Lessons.Update(lesson);
                db.SaveChanges();
                ViewManager.LessonViewAdmin(botClient, db, message, LessonId);
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, "Ошибка ввода! " + ex.Message);
            }
        }
        //public static void EditHomeWorkDesc(TelegramBotClient botClient, TelegramContext db, Message message, Guid HomeWorkId)
        //{
        //    var homeWork = db.HomeWorks.Where(x => x.Id == HomeWorkId).Single();
        //    homeWork.Description = message.Text;
        //    db.HomeWorks.Update(homeWork);
        //    db.SaveChanges();
        //    ViewManager.HomeWorkView(botClient, db, message, HomeWorkId);
        //}
        public static async void EditLessonSeqNumber(TelegramBotClient botClient, TelegramContext db, Message message, Guid LessonId, bool isNewLesson = false, int NewSeqNumber = 0)
        {
            try
            {
                if (NewSeqNumber == 0)
                {
                    NewSeqNumber = Convert.ToInt32(message.Text);
                }
                var lesson = db.Lessons.Where(x => x.Id == LessonId).Include(x=> x.CourseType).Single();
                lesson.SeqNumber = NewSeqNumber;
                db.Lessons.Update(lesson);
                db.SaveChanges();
                var lessons = db.Lessons.Where(x => x.Id != LessonId && x.SeqNumber >= NewSeqNumber && lesson.CourseType == x.CourseType).ToList();
                int i = 1;
                foreach (var less in lessons)
                {
                    less.SeqNumber = NewSeqNumber + i;
                }
                db.Lessons.UpdateRange(lessons);
                db.SaveChanges();
                if (!isNewLesson)
                {
                    await botClient.SendTextMessageAsync(message.Chat.Id, "Номер урока изменён.");
                }
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, "Ошибка изменения номера урока. " + ex.Message);
            }   
            
        }
        #endregion

        #region Delete
        public static async void DeleteLesson(TelegramBotClient botClient, TelegramContext db, Message message, Guid LessonId, bool FromTarif = false)
        {
            try
            {
                db.DataFiles.RemoveRange(db.DataFiles.Where(x => x.ParentId == LessonId).ToList());
                //var homeWorkReq = db.HomeWorkRequests.Where(x => x.Lesson.Id == LessonId).ToList();
                //if(homeWorkReq != null)
                //{
                //    foreach (var hmr in homeWorkReq)
                //    {
                //        db.DataFiles.RemoveRange(db.DataFiles.Where(x => x.ParentId == hmr.Id).ToList());
                //    }
                //    db.HomeWorkRequests.RemoveRange(homeWorkReq);
                //}

                //var homeWork = (db.HomeWorks.Where(x => x.Lesson.Id == LessonId).Single());
                //if(homeWork != null)
                //{
                //    db.DataFiles.RemoveRange(db.DataFiles.Where(x => x.ParentId == homeWork.Id));
                //    db.HomeWorks.Remove(homeWork);
                //}
                var delLesson = db.Lessons.Where(x => x.Id == LessonId).Single();
                //var newLesson = db.Lessons.Where(x => x.SeqNumber >= delLesson.SeqNumber + 1).Single();
                //EditLessonSeqNumber(botClient, db, message, newLesson.Id, true, delLesson.SeqNumber);
                db.Lessons.Remove(delLesson);
                db.SaveChanges();
                if (!FromTarif)
                {
                    await botClient.SendTextMessageAsync(message.Chat.Id, "Урок удалён");
                }
            }
            catch(Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, "Ошибка удаления урока. " + ex.Message);
            }
        }
        public static async void DeleteTarif(TelegramBotClient botClient, TelegramContext db, Message message, Guid TarifId, bool FromCourse = false)
        {
            try
            {
                var lessons = db.Lessons.Where(x => x.CourseType.Id == TarifId).ToList();
                if(lessons != null)
                {
                    db.Lessons.RemoveRange(lessons);
                }
                db.CoursesType.Remove(db.CoursesType.Where(x => x.Id == TarifId).Single());
                db.SaveChanges();
                if (!FromCourse)
                {
                    await botClient.SendTextMessageAsync(message.Chat.Id, "Тариф удалён");
                }
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, "Ошибка удаления тарифа. " + ex.Message);
            }
        }

        public static async void DeleteCourse(TelegramBotClient botClient, TelegramContext db, Message message, Guid CourseId)
        {
            try
            {
                
                var tarifList = db.CoursesType.Where(x => x.Course.Id == CourseId).ToList();
                if (tarifList != null)
                {
                    foreach (var tarif in tarifList)
                    {
                        DeleteTarif(botClient, db, message, tarif.Id, true);
                    }
                }
                db.Courses.Remove(db.Courses.Where(x => x.Id == CourseId).Single());
                db.SaveChanges();
                await botClient.SendTextMessageAsync(message.Chat.Id, "Курс удалён");
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, "Ошибка удаления курса. " + ex.Message);
            }
        }

        #endregion
    }
}
