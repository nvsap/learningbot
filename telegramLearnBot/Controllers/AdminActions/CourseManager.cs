﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using telegramLearnBot.Models;
using telegramLearnBot.Models.LearningModels;

namespace telegramLearnBot.Controllers.AdminActions
{
    public class CourseManager
    {
        #region CreateCourse
        public static async void CreateCourses(TelegramBotClient botClient, TelegramContext db, Message message)
        {
            await botClient.SendTextMessageAsync(message.Chat.Id, "Введите название курса." + "\n\n Что бы закончить создание курса введите команду /exit", Telegram.Bot.Types.Enums.ParseMode.Markdown);
            StateController.StateUpdate(message.Chat.Id, (int)AdminState.AddCourseName, true);//AddCourseName
        }
        public static async void AddCoursesName(TelegramBotClient botClient, TelegramContext db, Message message)
        {
            try
            {
                var courses = db.Courses.Where(x => x.Name == message.Text).ToList();
                if (courses.Count > 0)
                {
                    await botClient.SendTextMessageAsync(message.Chat.Id, "Такой курс уже есть, введите другое название!", Telegram.Bot.Types.Enums.ParseMode.Default);
                }
                else
                {
                    Course course = new Course { Id = Guid.NewGuid(), Name = message.Text };
                    db.Courses.Add(course);
                    StateController.StateUpdate(message.From.Id, (int)AdminState.AddCourseDescription, true, course.Id);
                    db.SaveChanges();
                    await botClient.SendTextMessageAsync(message.Chat.Id, "Введите описание курса", Telegram.Bot.Types.Enums.ParseMode.Default);
                }
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, "Ошибка: " + ex.Message, Telegram.Bot.Types.Enums.ParseMode.Default);
            }
        }

        public static async void AddCourseDescription(TelegramBotClient botClient, TelegramContext db, Message message, Guid CourseId)
        {
            try
            {
                var course = db.Courses.Where(x => x.Id == CourseId).ToList();
                course[0].Description = message.Text;
                db.Courses.Update(course[0]);
                db.SaveChanges();
                var keyboard = new InlineKeyboardMarkup(
                                            new InlineKeyboardButton[]
                                            {
                                            // First column
                                            new InlineKeyboardButton{ Text = "Да", CallbackData = "CreateDemoTrue"},
                                            new InlineKeyboardButton{ Text = "Нет", CallbackData = "CreateDemoFalse"}
                                            }
                                        );

                await botClient.SendTextMessageAsync(message.Chat.Id, "Создать демо тариф?", Telegram.Bot.Types.Enums.ParseMode.Default, false, false, 0, keyboard);
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, "Ошибка: " + ex.Message, Telegram.Bot.Types.Enums.ParseMode.Default);
            }
        }

        public static async void AddCourseType(TelegramBotClient botClient, TelegramContext db, Message message, Guid CourseId, bool isDemo = false)
        {
            try
            {
                var course = db.Courses.Where(x => x.Id == CourseId).ToList();
                CourseType courseType = new CourseType { Id = Guid.NewGuid(), Course = course[0], IsDemo = isDemo };
                db.CoursesType.Add(courseType);
                db.SaveChanges();
                //if (isDemo)
                //{
                    StateController.StateUpdate(message.Chat.Id, (int)AdminState.AddTarifName, true, courseType.Id, true, false, true);
                    await botClient.SendTextMessageAsync(message.Chat.Id, "Введите название тарифа", Telegram.Bot.Types.Enums.ParseMode.Default);
                //}
                //else
                //{
                //    var keyboard = new InlineKeyboardMarkup(
                //                            new InlineKeyboardButton[]
                //                            {
                //                            // First column
                //                            new InlineKeyboardButton{ Text = "Да", CallbackData = "UseMentorTrue"},
                //                            new InlineKeyboardButton{ Text = "Нет", CallbackData = "UseMentorFalse"}
                //                            }
                //                        );
                //    StateController.StateUpdate(message.Chat.Id, (int)AdminState.AddTarifName, true, courseType.Id, true, false, true);
                //    await botClient.SendTextMessageAsync(message.Chat.Id, "Включить поодержку ментора в курс? (добавить домашние задания)", Telegram.Bot.Types.Enums.ParseMode.Default, false, false, 0, keyboard);
                //}
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, "Ошибка: " + ex.Message, Telegram.Bot.Types.Enums.ParseMode.Default);
            }
        }

        public static async void AddCourseTypeName(TelegramBotClient botClient, TelegramContext db, Message message, Guid CourseId, Guid CourseTypeId)
        {
            try
            {
                var courseType = db.CoursesType.Where(x => x.Id == CourseTypeId).ToList();
                courseType[0].Name = message.Text;
                db.CoursesType.Update(courseType[0]);
                db.SaveChanges();
                StateController.StateUpdate(message.Chat.Id, (int)AdminState.AddTarifDescription, true, CourseTypeId, true, false);
                await botClient.SendTextMessageAsync(message.Chat.Id, "Введите описание название тарифа", Telegram.Bot.Types.Enums.ParseMode.Default);
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, "Ошибка: " + ex.Message, Telegram.Bot.Types.Enums.ParseMode.Default);
            }
        }
        public static async void AddCourseTypeDescription(TelegramBotClient botClient, TelegramContext db, Message message, Guid CourseId, Guid CourseTypeId)
        {
            try
            {
                var courseType = db.CoursesType.Where(x => x.Id == CourseTypeId).ToList();
                courseType[0].Description = message.Text;
                db.CoursesType.Update(courseType[0]);
                db.SaveChanges();
                if (courseType[0].IsDemo)
                {
                    StateController.StateUpdate(message.Chat.Id, (int)AdminState.AddLessons, true, CourseTypeId, true, false);
                    await botClient.SendTextMessageAsync(message.Chat.Id, "Введите название урока", Telegram.Bot.Types.Enums.ParseMode.Default);
                }
                else
                {
                    StateController.StateUpdate(message.Chat.Id, (int)AdminState.AddTarifAmount, true, CourseTypeId, true, false);
                    await botClient.SendTextMessageAsync(message.Chat.Id, "Введите цену тарифа", Telegram.Bot.Types.Enums.ParseMode.Default);
                }
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, "Ошибка: " + ex.Message, Telegram.Bot.Types.Enums.ParseMode.Default);
            }
        }
        public static async void AddCourseTypePrice(TelegramBotClient botClient, TelegramContext db, Message message, Guid CourseId, Guid CourseTypeId)
        {
            try
            {
                var courseType = db.CoursesType.Where(x => x.Id == CourseTypeId).ToList();
                courseType[0].PaymentAmount = Decimal.Parse(message.Text);
                db.CoursesType.Update(courseType[0]);
                db.SaveChanges();
                StateController.StateUpdate(message.Chat.Id, (int)AdminState.AddLessons, true, CourseTypeId, true, false);
                await botClient.SendTextMessageAsync(message.Chat.Id, "Введите название урока", Telegram.Bot.Types.Enums.ParseMode.Default);
            }
            catch(Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, ex.Message, Telegram.Bot.Types.Enums.ParseMode.Default);
            }
        }
        public static async void AddLessonName(TelegramBotClient botClient, TelegramContext db, Message message, Guid CourseId, Guid CourseTypeId)
        {
            try
            {
                var courseType = db.CoursesType.Where(x => x.Id == CourseTypeId).ToList();
                var state = db.UserStates.Where(x => x.CourseTypeAtWorkId == CourseTypeId).ToList();
                Lesson lesson = new Lesson { Id = Guid.NewGuid(), Name = message.Text, CourseType = courseType[0], HasHomework = courseType[0].IsUseMentor, SeqNumber = state[0].LessonsSeqNumber };
                ++state[0].LessonsSeqNumber;
                db.Lessons.Add(lesson);
                db.UserStates.Update(state[0]);
                db.SaveChanges();
                StateController.StateUpdate(message.Chat.Id, (int)AdminState.AddLessonsDescription, true, lesson.Id, false, true);
                await botClient.SendTextMessageAsync(message.Chat.Id, "Введите описание урока", Telegram.Bot.Types.Enums.ParseMode.Default);
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, "Ошибка: " + ex.Message, Telegram.Bot.Types.Enums.ParseMode.Default);
            }
        }
        public static async void AddLessonName(TelegramBotClient botClient, TelegramContext db, Message message, Guid CourseTypeId)
        {
            try
            {
                var courseType = db.CoursesType.Where(x => x.Id == CourseTypeId).Single();
                var lessonsCount = db.Lessons.Count(x => x.CourseType == courseType);
                var state = db.UserStates.Where(x => x.CourseTypeAtWorkId == CourseTypeId).ToList();
                Lesson lesson = new Lesson { Id = Guid.NewGuid(), Name = message.Text, CourseType = courseType, HasHomework = courseType.IsUseMentor, SeqNumber = lessonsCount + 1 };
                state[0].LessonsSeqNumber = lessonsCount + 2;
                db.Lessons.Add(lesson);
                db.UserStates.Update(state[0]);
                db.SaveChanges();
                StateController.StateUpdate(message.Chat.Id, (int)AdminState.AddLessonsDescription, true, lesson.Id, false, true);
                await botClient.SendTextMessageAsync(message.Chat.Id, "Введите описание урока", Telegram.Bot.Types.Enums.ParseMode.Default);
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, "Ошибка: " + ex.Message, Telegram.Bot.Types.Enums.ParseMode.Default);
            }
        }
        public static async void AddLessonName(TelegramBotClient botClient, TelegramContext db, Guid LessonId, Message message)
        {
            try
            {
                var lesson = db.Lessons.Where(x => x.Id == LessonId).Single();
                lesson.Name = message.Text;
                db.Lessons.Update(lesson);
                db.SaveChanges();
                StateController.StateUpdate(message.Chat.Id, (int)AdminState.AddLessonsDescription, true, lesson.Id, false, true);
                await botClient.SendTextMessageAsync(message.Chat.Id, "Введите описание урока", Telegram.Bot.Types.Enums.ParseMode.Default);
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, "Ошибка: " + ex.Message, Telegram.Bot.Types.Enums.ParseMode.Default);
            }
        }
        public static async void AddLesson(TelegramBotClient botClient, TelegramContext db, Message message, Guid CourseTypeId, int seqNumb)
        {
            try
            {
                var courseType = db.CoursesType.Where(x => x.Id == CourseTypeId).Single();
                var state = db.UserStates.Where(x => x.CourseTypeAtWorkId == CourseTypeId).ToList();
                Lesson lesson = new Lesson { Id = Guid.NewGuid(), CourseType = courseType, HasHomework = courseType.IsUseMentor, SeqNumber = seqNumb };
                state[0].LessonsSeqNumber = seqNumb + 1;
                db.Lessons.Add(lesson);
                db.UserStates.Update(state[0]);
                db.SaveChanges();
                EditManager.EditLessonSeqNumber(botClient, db, message, lesson.Id, true);
                StateController.StateUpdate(message.Chat.Id, (int)AdminState.AddLessonsName, true, lesson.Id, false, true);
                await botClient.SendTextMessageAsync(message.Chat.Id, "Следующие уроки будут нумерироватся от этого. \n \n Введите название урока", Telegram.Bot.Types.Enums.ParseMode.Default);
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, "Ошибка: " + ex.Message, Telegram.Bot.Types.Enums.ParseMode.Default);
            }
        }
        public static async void AddLessonDescription(TelegramBotClient botClient, TelegramContext db, Message message, Guid CourseId, Guid CourseTypeId, Guid LessonId)
        {
            try
            {
                var lessons = db.Lessons.Where(x => x.Id == LessonId).ToList();
                lessons[0].Desciption = message.Text;
                db.Lessons.Update(lessons[0]);
                db.SaveChanges();
                StateController.StateUpdate(message.Chat.Id, (int)AdminState.AddLessonsTime, true, LessonId, false, true);
                await botClient.SendTextMessageAsync(message.Chat.Id, $"Введите дату и время публикации урока в фомате '2009-05-28 14:40'", Telegram.Bot.Types.Enums.ParseMode.Markdown);
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, "Ошибка: " + ex.Message, Telegram.Bot.Types.Enums.ParseMode.Default);
            }
        }
        public static async void AddLessonTime(TelegramBotClient botClient, TelegramContext db, Message message, Guid LessonId)
        {
            try
            {
                var lessons = db.Lessons.Where(x => x.Id == LessonId).ToList();
                lessons[0].PostTime = DateTime.ParseExact(message.Text, "yyyy-MM-dd HH:mm",
                                       System.Globalization.CultureInfo.InvariantCulture);
                lessons[0].IsPublished = false;
                db.Lessons.Update(lessons[0]);
                db.SaveChanges();
                StateController.StateUpdate(message.Chat.Id, (int)AdminState.AddLessonsFiles, true, LessonId, false, true);
                await botClient.SendTextMessageAsync(message.Chat.Id, $"Добавте файлы урока, объёмом меньше 20 мб. \n\n Когда закончите, введите команду:" +
                    $" \n /nextL - для создания следующего урока \n /nextT - для добавления нового тарифа в текущий курс \n /exit - для выхода в главное меню.", Telegram.Bot.Types.Enums.ParseMode.Markdown);
            
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, "Ошибка: " + ex.Message, Telegram.Bot.Types.Enums.ParseMode.Default);
            }
        }

        //public static async void AddHomeworkDescription(TelegramBotClient botClient, TelegramContext db, Message message, Guid HomeWorkAtWorkId)
        //{
        //    try
        //    {
        //        var homeWork = db.HomeWorks.Where(x => x.Id == HomeWorkAtWorkId).Single();
        //        homeWork.Description = message.Text;
        //        db.HomeWorks.Update(homeWork);
        //        db.SaveChanges();
        //        StateController.StateUpdate(message.Chat.Id, (int)AdminState.AddHomeworkFiles, true, HomeWorkAtWorkId, false, false);
        //        await botClient.SendTextMessageAsync(message.Chat.Id, $"Добавте файлы ДЗ, объёмом меньше 20 мб. \n\n Когда закончите, введите команду:" +
        //            $" \n /nextL - для создания следующего урока \n /nextT - для добавления нового тарифа в текущий курс \n /exit - для выхода в главное меню.", Telegram.Bot.Types.Enums.ParseMode.Markdown);
        //    }
        //    catch (Exception ex)
        //    {
        //        await botClient.SendTextMessageAsync(message.Chat.Id, $"Ошибка добавления ДЗ: \n" + ex.Message);
        //    }
        //}

        #endregion


        #region CoursesView
        public static async void CoursesListShow(TelegramBotClient botClient, TelegramContext db, Message message)
        {
            try
            { 
                var CoursesList = db.Courses.ToList();
                if(CoursesList.Count > 0)
                {
                    var keyboardInline = new InlineKeyboardButton[CoursesList.Count+1][];
                    int i = 0;
                    foreach (var course in CoursesList)
                    {
                        var name = course.Name == null ? "*empty course name*" : course.Name;
                        keyboardInline[i] = new[]
                        {
                        new InlineKeyboardButton { Text = name, CallbackData = "C" + course.Id }
                        };
                        i++;
                    }
                    keyboardInline[i] = new[]
                    {
                        new InlineKeyboardButton { Text = "Назад", CallbackData = "MainMenu"},
                    };
                    var keyboard = new InlineKeyboardMarkup(keyboardInline);
                    await botClient.SendTextMessageAsync(message.Chat.Id, "Список курсов", Telegram.Bot.Types.Enums.ParseMode.Default, false, false, 0, keyboard);
                }
                else
                {
                    await botClient.SendTextMessageAsync(message.Chat.Id, "Курсов нет(", Telegram.Bot.Types.Enums.ParseMode.Default);
                }
                
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, $"Ошибка: \n" + ex.Message);
            }

        }
        public static async void TarifListShow(TelegramBotClient botClient, TelegramContext db, Message message, Course course)
        {
            try
            {
                var TarifList = db.CoursesType.Where(x => x.Course == course).ToList();
                var keyboardInline = new InlineKeyboardButton[TarifList.Count + 2][];
                int i = 0;
                foreach (var tarif in TarifList)
                {
                    var name = tarif.Name == null ? "*empty tarif name*" : tarif.Name;
                    keyboardInline[i] = new[]
                        {
                            new InlineKeyboardButton { Text = name, CallbackData = "T" + tarif.Id }
                        };
                    i++;
                }
                keyboardInline[TarifList.Count] = new[]
                    {
                        new InlineKeyboardButton { Text = "Редактировать курс", CallbackData = "Ec" + course.Id }
                    };
                keyboardInline[TarifList.Count + 1] = new[]
                    {
                        new InlineKeyboardButton { Text = "Назад", CallbackData = "CoursesList" }
                    };
                var keyboard = new InlineKeyboardMarkup(keyboardInline);
                await botClient.SendTextMessageAsync(message.Chat.Id, "Список тарифов курса *" + course.Name + "*", Telegram.Bot.Types.Enums.ParseMode.Markdown, false, false, 0, keyboard);
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, $"Ошибка: \n" + ex.Message);
            }
        }
        public static async void EditMenuCourse(TelegramBotClient botClient, TelegramContext db, Message message, Course course)
        {
            try
            {
                
                //StateController.StateUpdate(message.Chat.Id, (int)AdminState.AddTarifName, true, course.Id);
                var keyboardInline = new InlineKeyboardButton[6][];
                keyboardInline[0] = new[]
                {
                    new InlineKeyboardButton { Text = "Изменить название курса", CallbackData = "ecn" + course.Id }
                };
                keyboardInline[1] = new[]
                {
                    new InlineKeyboardButton { Text = "Изменить описание курса", CallbackData = "ecd" + course.Id }
                };
                keyboardInline[2] = new[]
                {
                    new InlineKeyboardButton { Text = "Добавить тариф", CallbackData = "at" + course.Id }
                };
                keyboardInline[3] = new[]
                    {
                    new InlineKeyboardButton { Text = "Удалить курс", CallbackData = "dc" + course.Id }
                };
                keyboardInline[4] = new[]
                {
                    new InlineKeyboardButton { Text = "Отображать для новых юзеров (" + (!course.IsHiden).ToString() + ")", CallbackData = "ch" + course.Id }
                };
                keyboardInline[5] = new[]
                {
                    new InlineKeyboardButton { Text = "Назад", CallbackData = "C" + course.Id }
                };
                var keyboard = new InlineKeyboardMarkup(keyboardInline);
                await botClient.SendTextMessageAsync(message.Chat.Id, "Редактировать курс *" + course.Name + "* \n\n" + course.Description + "", Telegram.Bot.Types.Enums.ParseMode.Markdown, false, false, 0, keyboard);
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, $"Ошибка: \n" + ex.Message);
            }
        }
        public static async void EditCourseVisibilty(TelegramBotClient botClient, TelegramContext db, Message message, Guid courseId)
        {
            try
            {
                var course = db.Courses.Where(x => x.Id == courseId).SingleOrDefault();
                var mess = course.IsHiden ? "Открыть" : "Закрыть";
                var keyboardInline = new InlineKeyboardButton[2][];
                keyboardInline[0] = new[]
                    {
                    new InlineKeyboardButton { Text = mess, CallbackData = "cs" + course.Id }
                };
                keyboardInline[1] = new[]
                    {
                    new InlineKeyboardButton { Text = "Назад", CallbackData = "C" + course.Id }
                };
                var keyboard = new InlineKeyboardMarkup(keyboardInline);
                await botClient.SendTextMessageAsync(message.Chat.Id, "Доступ к курсу " + course.Name + " для новых пользователей " + (course.IsHiden ? "закрыт." : "открыт."), Telegram.Bot.Types.Enums.ParseMode.Markdown, false, false, 0, keyboard);
                
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, $"Ошибка: \n" + ex.Message);
            }
        }
        public static async void LessonsMainListShow(TelegramBotClient botClient, TelegramContext db, Message message, CourseType courseType)
        {
            try
            {
                var LessonsList = db.Lessons.Where(x => x.CourseType == courseType).OrderBy(l => l.SeqNumber).ToList();
                var keyboardInline = new InlineKeyboardButton[LessonsList.Count + 2][];
                int i = 0;
                foreach (var lesson in LessonsList)
                {
                    var name = lesson.Name == null ? "*empty name*" : lesson.Name;
                    keyboardInline[i] = new[]
                    {
                    new InlineKeyboardButton { Text = lesson.SeqNumber + ". " + name, CallbackData = "L" + lesson.Id }
                    };
                    i++;
                }
                keyboardInline[LessonsList.Count] = new[]
                    {
                    new InlineKeyboardButton { Text = "Редактировать тариф.", CallbackData = "Et" + courseType.Id }
                };
                keyboardInline[LessonsList.Count + 1] = new[]
                    {
                    new InlineKeyboardButton { Text = "Назад", CallbackData = "C" + courseType.Course.Id }
                };
                var keyboard = new InlineKeyboardMarkup(keyboardInline);
                if (courseType.IsDemo)
                {
                    await botClient.SendTextMessageAsync(message.Chat.Id, "Список уроков тарифа *" + courseType.Name + "*. Демо тариф.", Telegram.Bot.Types.Enums.ParseMode.Markdown, false, false, 0, keyboard);
                }
                else if(courseType.IsUseMentor)
                {
                    await botClient.SendTextMessageAsync(message.Chat.Id, "Список уроков тарифа *" + courseType.Name + "*. Поддержка ментора.", Telegram.Bot.Types.Enums.ParseMode.Markdown, false, false, 0, keyboard);
                }
                else
                {
                    await botClient.SendTextMessageAsync(message.Chat.Id, "Список уроков тарифа *" + courseType.Name + "*", Telegram.Bot.Types.Enums.ParseMode.Markdown, false, false, 0, keyboard);
                }
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, $"Ошибка: \n" + ex.Message);
            }
        }
        public static async void EditMenuTarif(TelegramBotClient botClient, TelegramContext db, Message message, CourseType courseType)
        {
            try
            {
                var keyboardInline = new InlineKeyboardButton[6][];
                keyboardInline[0] = new[]
                    {
                    new InlineKeyboardButton { Text = "Изменить название", CallbackData = "etn" + courseType.Id }
                };
                keyboardInline[1] = new[]
                    {
                    new InlineKeyboardButton { Text = "Изменить описание ", CallbackData = "etd" + courseType.Id }
                };
                keyboardInline[2] = new[]
                    {
                    new InlineKeyboardButton { Text = "Изменить цену", CallbackData = "etp" + courseType.Id }
                };
                keyboardInline[3] = new[]
                    {
                    new InlineKeyboardButton { Text = "Добавить урок", CallbackData = "al" + courseType.Id }
                };
                keyboardInline[4] = new[]
                    {
                    new InlineKeyboardButton { Text = "Удалить тариф", CallbackData = "dt" + courseType.Id }
                };
                keyboardInline[5] = new[]
                    {
                    new InlineKeyboardButton { Text = "Назад", CallbackData = "T" + courseType.Id }
                };
                var keyboard = new InlineKeyboardMarkup(keyboardInline);
                await botClient.SendTextMessageAsync(message.Chat.Id, "Редактировать тариф *" + courseType.Name + "*. \n\n" + courseType.Description + " \n\n Изменение цены для демо тарифа не поддерживается.", Telegram.Bot.Types.Enums.ParseMode.Markdown, false, false, 0, keyboard);
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, $"Ошибка: \n" + ex.Message);
            }
        }
        public static async void LessonHomeworkShow(TelegramBotClient botClient, TelegramContext db, Message message, Lesson lesson)
        {
            try
            {
                var keyboardInline = new InlineKeyboardButton[3][];
                //var homeWork = db.HomeWorks.Where(x => x.Lesson == lesson).SingleOrDefault();

                //if (lesson.HasHomework)
                //{
                //    if (homeWork != null)
                //    {
                //        keyboardInline[0] = new[]
                //            {
                //            new InlineKeyboardButton { Text = lesson.Name, CallbackData = "l" + lesson.Id },
                //            new InlineKeyboardButton { Text = "ДЗ " + lesson.Name, CallbackData = "h" + homeWork.Id }
                //        };
                //    }
                //    else
                //    {
                //        keyboardInline[0] = new[]
                //            {
                //            new InlineKeyboardButton { Text = lesson.Name, CallbackData = "l" + lesson.Id },
                //            new InlineKeyboardButton { Text = "Добавить ДЗ", CallbackData = "alh" + lesson.Id }
                //        };
                //    }
                //}
                //else
                //{
                var name = lesson.Name == null ? "*empty name*" : lesson.Name;
                keyboardInline[0] = new[]
                    {
                        new InlineKeyboardButton { Text = name, CallbackData = "l" + lesson.Id },
                    };
                //}
                keyboardInline[1] = new[]
                    {
                        new InlineKeyboardButton { Text = "Изменить номер", CallbackData = "s" + lesson.Id },
                        new InlineKeyboardButton { Text = "Удалить урок", CallbackData = "dl" + lesson.Id },
                    };
                keyboardInline[2] = new[]
                    {
                        new InlineKeyboardButton { Text = "Назад", CallbackData = "T" + lesson.CourseType.Id }
                    };
                var keyboard = new InlineKeyboardMarkup(keyboardInline);
                await botClient.SendTextMessageAsync(message.Chat.Id, "Урок №" + lesson.SeqNumber + ". *" + name + "*", Telegram.Bot.Types.Enums.ParseMode.Markdown, false, false, 0, keyboard);
            }
            catch (Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, $"Ошибка: \n" + ex.Message);
            }
            

        }
        #endregion
    }
}
