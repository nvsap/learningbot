﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using telegramLearnBot.Models;

namespace telegramLearnBot.Controllers
{
    [Route("api/message/update")]
    public class MessageController : Controller
    {
        // GET api/values/5
        [HttpGet]
        public string Get()
        {
            return "Method GET unuvalable";
        }

        [HttpPost]
        public async Task<OkResult> Post([FromBody]Update update)
        {
            
                if (update == null) return Ok();

                var commands = Bot.Commands;
                var message = update.Message;
                var botClient = await Bot.GetBotClientAsync();

            try
            {
                if (update.Type == UpdateType.Message)
                {

                    foreach (var command in commands)
                    {
                        if (command.Name == message.Text)
                        {
                            await command.Execute(message, botClient);
                            return Ok();
                        }
                    }
                    if (!String.IsNullOrEmpty(message.Text))
                    {
                        if (message.Text.Contains("/payment"))
                        {
                            //TODO: select a user payment
                            ViewManager.PaymentSelect(botClient, message);
                            return Ok();
                        }
                    }
                    StateController.StateControl(botClient, update);
                }
                else if (update.Type == UpdateType.CallbackQuery)
                {
                    CallbackQueryController.ChoseCallBackQuery(botClient, update);
                }
                return Ok();
            }
            catch(Exception ex)
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, "Возникла ошибка. Обратитесь к администратору с следующим текстом: " + ex.Message, Telegram.Bot.Types.Enums.ParseMode.Default);
                return Ok();
            }
        }

        public static async void MainMenu(Models.LearningModels.User user, TelegramContext db, Message message, Telegram.Bot.TelegramBotClient botClient)
        {
            if (user.IsAdmin)
            {
                Console.WriteLine("Send message to master-" + user.Name);
                var keyboardInline = new InlineKeyboardButton[3][];
                keyboardInline[0] = new[]
                    {
                    new InlineKeyboardButton{Text = "Список курсов", CallbackData = "CoursesList" },
                     new InlineKeyboardButton{Text = "Добавить курс", CallbackData = "AddCourse" },
                    };
                keyboardInline[1] = new[]
                    {
                        new InlineKeyboardButton{Text = "Список вебинаров", CallbackData = "WebinarsList"},
                        new InlineKeyboardButton{Text = "Добавить вебинар", CallbackData = "AddWebinar"}
                    };
                keyboardInline[2] = new[]
                    {
                        new InlineKeyboardButton{Text = "Отправить сообщение всем пользователям", CallbackData = "SendMessageToAll"},
                        new InlineKeyboardButton{Text = "Проплата", CallbackData = "Payment"}
                    };
                var keyboard = new InlineKeyboardMarkup(keyboardInline);
                await botClient.SendTextMessageAsync(message.Chat.Id, "Wellcome, master - " + user.Name + "!", Telegram.Bot.Types.Enums.ParseMode.Default, false, false, 0, keyboard);

                StateController.StateUpdate(user.UserId, (int)Models.LearningModels.AdminState.CallBackMainMenu, user.IsAdmin);
            }
            else
            {
                //var keyboard = new InlineKeyboardMarkup(
                //                    new InlineKeyboardButton[]
                //                    {
                //                            // First column
                //                            new InlineKeyboardButton{ Text = "Записаться на вебинар", CallbackData = "WebinarReg"}

                //                    }
                //                );
                var keyboard = new InlineKeyboardMarkup(
                                    new InlineKeyboardButton[]
                                    {
                                            // First column
                                            new InlineKeyboardButton{ Text = "Новый курс", CallbackData = "NewCoursesList"},

                                            // Second column
                                            new InlineKeyboardButton{ Text = "Мои курсы", CallbackData = "MyCoursesList"}

                                    }
                                );

                var mess = await botClient.SendTextMessageAsync(message.Chat.Id, "Салют, " + message.Chat.FirstName +
                "! \n\nЗдесь ты можешь изучить что-нибудь новенькое и полезное. Доступных материалов пока немного, но они будут пополняться в ближайшее время🦄", Telegram.Bot.Types.Enums.ParseMode.Default, false, false, 0, keyboard);
                StateController.SetLastBotMessageId(message.Chat.Id, mess.MessageId);
            }
        }
    }
}
