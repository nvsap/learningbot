﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Telegram.Bot.Types;
using telegramLearnBot.Models;
using telegramLearnBot.Controllers.AdminActions;
using telegramLearnBot.Models.LearningModels;
using Telegram.Bot.Types.ReplyMarkups;
using Microsoft.EntityFrameworkCore;

namespace telegramLearnBot.Controllers
{
    public class CallbackQueryController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public static async void ChoseCallBackQuery(Telegram.Bot.TelegramBotClient botClient, Update Update)
        {
            try
            {
                using (TelegramContext db = new TelegramContext())
                {
                    var update = Update;
                    var user = db.Users.Where(x => x.UserId == update.CallbackQuery.From.Id).Single();
                    var callBack = update.CallbackQuery;
                    await botClient.AnswerCallbackQueryAsync(update.CallbackQuery.Id);
                    if (user.IsAdmin)
                    {
                        if (callBack.Data == "WebinarsList")
                        {
                            WebinarsManager.WebinarsListShow(botClient, db, update.CallbackQuery.Message);
                        }

                        else if (callBack.Data == "AddWebinar")
                        {
                            //await botClient.SendTextMessageAsync(update.CallbackQuery.Message.Chat.Id, "тест", replyToMessageId: update.CallbackQuery.Message.MessageId);
                            WebinarsManager.CreateWebinars(botClient, db, update.CallbackQuery.Message);
                        }
                        if (callBack.Data == "CoursesList")
                        {
                            CourseManager.CoursesListShow(botClient, db, update.CallbackQuery.Message);
                        }

                        else if (callBack.Data == "AddCourse")
                        {
                            //await botClient.SendTextMessageAsync(update.CallbackQuery.Message.Chat.Id, "тест", replyToMessageId: update.CallbackQuery.Message.MessageId);
                            CourseManager.CreateCourses(botClient, db, update.CallbackQuery.Message);
                        }
                        else if (callBack.Data == "MainMenu")
                        {
                            //await botClient.SendTextMessageAsync(update.CallbackQuery.Message.Chat.Id, "тест", replyToMessageId: update.CallbackQuery.Message.MessageId);
                            MessageController.MainMenu(user, db, update.CallbackQuery.Message, botClient);
                        }
                        else if (callBack.Data == "Payment")
                        {
                            //await botClient.SendTextMessageAsync(update.CallbackQuery.Message.Chat.Id, "тест", replyToMessageId: update.CallbackQuery.Message.MessageId);
                            var keyboardInline = new InlineKeyboardButton[2][];
                            keyboardInline[0] = new InlineKeyboardButton[]
                            {
                                new InlineKeyboardButton { Text = "Список оплат", CallbackData = "PaymentList"},
                                new InlineKeyboardButton { Text = "Поиск оплат", CallbackData = "PaymentSearch"}
                            };
                            keyboardInline[1] = new InlineKeyboardButton[]
                            {
                                new InlineKeyboardButton { Text = "Назад", CallbackData = "MainMenu" }
                            };
                            var keyboard = new InlineKeyboardMarkup(keyboardInline);
                            await botClient.SendTextMessageAsync(update.CallbackQuery.Message.Chat.Id, "*Проплата*", Telegram.Bot.Types.Enums.ParseMode.Markdown, false, false, 0, keyboard);
                        }
                        else if (callBack.Data == "PaymentList")
                        {
                            ViewManager.PaymentsListInitialize(botClient, db, update.CallbackQuery.Message, 0);
                        }
                        else if (callBack.Data == "PaymentSearch")
                        {
                            //TODO
                            StateController.StateUpdate(user.UserId, (int)AdminState.PaymentSearch, true);
                            await botClient.SendTextMessageAsync(update.CallbackQuery.Message.Chat.Id, "Введите ник пользователя.", Telegram.Bot.Types.Enums.ParseMode.Markdown);
                        }
                        else if (callBack.Data == "CreateDemoTrue")
                        {
                            var userState = db.UserStates.Where(x => x.User.UserId == callBack.From.Id).ToList();
                            CourseManager.AddCourseType(botClient, db, update.CallbackQuery.Message, userState[0].CourseAtWorkId, true);
                        }
                        else if (callBack.Data == "CreateDemoFalse")
                        {
                            var userState = db.UserStates.Where(x => x.User.UserId == callBack.From.Id).ToList();
                            CourseManager.AddCourseType(botClient, db, callBack.Message, userState[0].CourseAtWorkId, false);
                        }
                        else if (callBack.Data == "UseMentorTrue")
                        {
                            var userState = db.UserStates.Where(x => x.User.UserId == callBack.From.Id).ToList();
                            var courseType = db.CoursesType.Where(x => x.Id == userState[0].CourseTypeAtWorkId).ToList();
                            courseType[0].IsUseMentor = true;
                            db.CoursesType.Update(courseType[0]);
                            db.SaveChanges();
                            StateController.StateUpdate(callBack.Message.Chat.Id, (int)AdminState.AddTarifName, true, courseType[0].Id, true, false);
                            await botClient.SendTextMessageAsync(callBack.Message.Chat.Id, "Введите название тарифа", Telegram.Bot.Types.Enums.ParseMode.Default);

                        }
                        else if (callBack.Data == "UseMentorFalse")
                        {
                            var userState = db.UserStates.Where(x => x.User.UserId == callBack.From.Id).ToList();
                            var courseType = db.CoursesType.Where(x => x.Id == userState[0].CourseTypeAtWorkId).ToList();
                            courseType[0].IsUseMentor = false;
                            db.CoursesType.Update(courseType[0]);
                            db.SaveChanges();
                            StateController.StateUpdate(callBack.Message.Chat.Id, (int)AdminState.AddTarifName, true, courseType[0].Id, true, false);
                            await botClient.SendTextMessageAsync(callBack.Message.Chat.Id, "Введите название тарифа", Telegram.Bot.Types.Enums.ParseMode.Default);
                        }
                        else
                        {
                            var callBackSign = callBack.Data.First();
                            var trimCallBack = callBack.Data.Remove(0, 1);
                            if (callBackSign == 'C')
                            {
                                var Course = db.Courses.Where(x => x.Id == Guid.Parse(trimCallBack)).Single();
                                CourseManager.TarifListShow(botClient, db, update.CallbackQuery.Message, Course);
                            }
                            else if (callBackSign == 'P')
                            {
                                if (trimCallBack.First() == 'L')
                                {
                                    ViewManager.PaymentsListInitialize(botClient, db, update.CallbackQuery.Message, int.Parse(trimCallBack.Remove(0, 1)));
                                }
                            }
                            if (callBackSign == 'G')
                            {
                                var trimToGuid = trimCallBack.Remove(0, 1);
                                if (trimCallBack.First() == 'A')
                                {
                                    Message mess;
                                    var Payment = db.Payments.Where(x => x.Id == Guid.Parse(trimToGuid)).Single();
                                    if (Payment.Status)
                                    {
                                        Payment.Status = false;
                                    }
                                    else
                                    {
                                        string CourseName = "";
                                        if(Payment.CourseTypeId != null)
                                        {
                                            var CourseType = db.CoursesType.Where(x => x.Id == Payment.CourseTypeId).Include(x => x.Course).Single();
                                            var progress = db.Progresses.Where(x => x.ProgressObjectId == CourseType.Id && x.User == update.CallbackQuery.Message.Chat.Id).SingleOrDefault();
                                            //if (progress == null)
                                            //{
                                            //    var Course = db.Courses.Where(x => x.Id == CourseType.Course.Id).Single();
                                            //    var Lesson = db.Lessons.Where(x => x.CourseType.Id == CourseType.Id).OrderBy(x => x.SeqNumber).Take(1).Single();
                                            //    StateController.AddProgress(CourseType.Id, (int)ProgressObject.Tarif, (int)Payment.UserId);
                                            //    StateController.AddProgress(Lesson.Id, (int)ProgressObject.Lesson, (int)Payment.UserId);
                                            //}
                                            CourseName = CourseType.Name;
                                            Payment.Status = true;
                                            
                                        }
                                        else
                                        {
                                            var WebinarUser = db.WebinarsUsers.Where(x => x.User.UserId == Payment.UserId && x.Webinars.Id == Payment.WebinarId).Include(x => x.User).Include(x => x.Webinars).SingleOrDefault();
                                            if(WebinarUser == null)
                                            {
                                                WebinarUser = new WebinarsUsers()
                                                {
                                                    User = db.Users.Where(x => x.UserId == Payment.UserId).Take(1).SingleOrDefault(),
                                                    Webinars = db.Webinars.Where(x => x.Id == Payment.WebinarId).Take(1).SingleOrDefault()
                                                };
                                                db.WebinarsUsers.Add(WebinarUser);
                                            }
                                            CourseName = WebinarUser.Webinars.Name;
                                            Payment.Status = true;
                                        }
                                        var keyboardInline = new InlineKeyboardButton[1][];
                                        keyboardInline[0] = new[]
                                            {
                                                    new InlineKeyboardButton { Text = "Назад", CallbackData = "MainMenu"},
                                                };
                                        var keyboard = new InlineKeyboardMarkup(keyboardInline);
                                        mess = await botClient.SendTextMessageAsync(Payment.UserId, "Открылся доступ к «" + CourseName + "» 😌", Telegram.Bot.Types.Enums.ParseMode.Markdown, false, false, 0, keyboard);
                                        StateController.SetLastBotMessageId(Payment.UserId, mess.MessageId);
                                    }
                                    db.Update(Payment);
                                    db.SaveChanges();
                                    await botClient.SendTextMessageAsync(callBack.Message.Chat.Id, "Успешно сохранено.", Telegram.Bot.Types.Enums.ParseMode.Default);
                                }
                            }
                            else if (callBackSign == 'T')
                            {
                                var Tarif = db.CoursesType.Where(x => x.Id == Guid.Parse(trimCallBack)).Include(x => x.Course).Single();
                                CourseManager.LessonsMainListShow(botClient, db, update.CallbackQuery.Message, Tarif);
                            }
                            else if (callBackSign == 'L')
                            {
                                var Lesson = db.Lessons.Where(x => x.Id == Guid.Parse(trimCallBack)).Include(x => x.CourseType).Single();
                                CourseManager.LessonHomeworkShow(botClient, db, update.CallbackQuery.Message, Lesson);
                            }
                            else if (callBackSign == 'l')
                            {
                                ViewManager.LessonViewAdmin(botClient, db, update.CallbackQuery.Message, Guid.Parse(trimCallBack));
                            }
                            //else if (callBackSign == 'h')
                            //{
                            //    ViewManager.HomeWorkView(botClient, db, update.CallbackQuery.Message, Guid.Parse(trimCallBack));
                            //}
                            else if (callBackSign == 'E')
                            {
                                var trimToGuid = trimCallBack.Remove(0, 1);
                                if (trimCallBack.First() == 'c')
                                {
                                    var Course = db.Courses.Where(x => x.Id == Guid.Parse(trimToGuid)).Single();
                                    CourseManager.EditMenuCourse(botClient, db, update.CallbackQuery.Message, Course);
                                }
                                else if (trimCallBack.First() == 't')
                                {
                                    var Tarif = db.CoursesType.Where(x => x.Id == Guid.Parse(trimToGuid)).Single();
                                    CourseManager.EditMenuTarif(botClient, db, update.CallbackQuery.Message, Tarif);
                                }
                            }
                            else if (callBackSign == 'W' && trimCallBack.Length == 36)
                            {
                                var webinar = db.Webinars.Where(x => x.Id == Guid.Parse(trimCallBack)).SingleOrDefault();
                                if (webinar != null)
                                {
                                    WebinarsManager.EditMenuWebinar(botClient, db, callBack.Message, webinar);
                                }
                            }
                            else if (callBackSign == 'c')
                            {
                                var trimToEditObject = trimCallBack.Remove(0, 1);
                                if (trimCallBack.First() == 'h')
                                {
                                    CourseManager.EditCourseVisibilty(botClient, db, callBack.Message, Guid.Parse(trimToEditObject));
                                }
                                else
                                {
                                    var course = db.Courses.Where(x => x.Id == Guid.Parse(trimToEditObject)).SingleOrDefault();
                                    course.IsHiden = !course.IsHiden;
                                    db.Courses.Update(course);
                                    db.SaveChanges();
                                    CourseManager.EditMenuCourse(botClient, db, callBack.Message, course);
                                }
                            }
                            else if (callBackSign == 'e')
                            {
                                var trimToEditObject = trimCallBack.Remove(0, 1);
                                var trimToGuid = trimToEditObject.Remove(0, 1);
                                if (trimCallBack.First() == 'c')
                                {

                                    if (trimToEditObject.First() == 'n')//edit course name
                                    {
                                        StateController.StateUpdate(callBack.From.Id, (int)AdminState.EditCourseName, true, Guid.Parse(trimToGuid));
                                        await botClient.SendTextMessageAsync(callBack.Message.Chat.Id, "Введите новое название курса", Telegram.Bot.Types.Enums.ParseMode.Default);
                                    }
                                    else//edit desc
                                    {
                                        StateController.StateUpdate(callBack.From.Id, (int)AdminState.EditCourseDescription, true, Guid.Parse(trimToGuid));
                                        await botClient.SendTextMessageAsync(callBack.Message.Chat.Id, "Введите новое описание курса", Telegram.Bot.Types.Enums.ParseMode.Default);
                                    }
                                }
                                else if (trimCallBack.First() == 't')
                                {
                                    if (trimToEditObject.First() == 'n')//edit tarif name
                                    {
                                        StateController.StateUpdate(callBack.From.Id, (int)AdminState.EditTarifName, true, Guid.Parse(trimToGuid), true, false);
                                        await botClient.SendTextMessageAsync(callBack.Message.Chat.Id, "Введите новое название тарифа", Telegram.Bot.Types.Enums.ParseMode.Default);
                                    }
                                    else if (trimToEditObject.First() == 'd')//edit desc
                                    {
                                        StateController.StateUpdate(callBack.From.Id, (int)AdminState.EditTarifDescription, true, Guid.Parse(trimToGuid), true, false);
                                        await botClient.SendTextMessageAsync(callBack.Message.Chat.Id, "Введите новое описание тарифа", Telegram.Bot.Types.Enums.ParseMode.Default);
                                    }
                                    else//edit tarif price
                                    {
                                        StateController.StateUpdate(callBack.From.Id, (int)AdminState.EditTarifPrice, true, Guid.Parse(trimToGuid), true, false);
                                        await botClient.SendTextMessageAsync(callBack.Message.Chat.Id, "Введите новую цену тарифа", Telegram.Bot.Types.Enums.ParseMode.Default);
                                    }
                                }
                                else if (trimCallBack.First() == 'l')
                                {
                                    if (trimToEditObject.First() == 'n')//edit lesson name
                                    {
                                        StateController.StateUpdate(callBack.From.Id, (int)AdminState.EditLessonName, true, Guid.Parse(trimToGuid), false, true);
                                        await botClient.SendTextMessageAsync(callBack.Message.Chat.Id, "Введите новое название урока", Telegram.Bot.Types.Enums.ParseMode.Default);
                                    }
                                    if (trimToEditObject.First() == 't' && trimToGuid.Length == 36)
                                    {
                                        StateController.StateUpdate(callBack.From.Id, (int)AdminState.EditLessonsTime, true, Guid.Parse(trimToGuid), false, true);
                                        await botClient.SendTextMessageAsync(callBack.Message.Chat.Id, "Введите новое время публикации урока. В формате yyyy-MM-dd HH:mm", Telegram.Bot.Types.Enums.ParseMode.Default);
                                    }
                                    else if (trimToEditObject.First() == 'd')//edit lesson desc
                                    {
                                        StateController.StateUpdate(callBack.From.Id, (int)AdminState.EditLessonDescription, true, Guid.Parse(trimToGuid), false, true);
                                        await botClient.SendTextMessageAsync(callBack.Message.Chat.Id, "Введите новое описание урока", Telegram.Bot.Types.Enums.ParseMode.Default);
                                    }
                                }
                                else if(trimCallBack.First() == 'w')
                                {
                                    if(trimToEditObject.First() == 'n' && trimToGuid.Length == 36)
                                    {
                                        StateController.StateUpdate(callBack.From.Id, (int)AdminState.EditWebinarName, true, Guid.Parse(trimToGuid));
                                        await botClient.SendTextMessageAsync(callBack.Message.Chat.Id, "Введите новое название вебинара", Telegram.Bot.Types.Enums.ParseMode.Default);
                                    }

                                    if (trimToEditObject.First() == 'd' && trimToGuid.Length == 36)
                                    {
                                        StateController.StateUpdate(callBack.From.Id, (int)AdminState.EditWebinarDescription, true, Guid.Parse(trimToGuid));
                                        await botClient.SendTextMessageAsync(callBack.Message.Chat.Id, "Введите новое описание вебинара", Telegram.Bot.Types.Enums.ParseMode.Default);

                                    }
                                    if (trimToEditObject.First() == 't' && trimToGuid.Length == 36)
                                    {
                                        StateController.StateUpdate(callBack.From.Id, (int)AdminState.EditWebinarDate, true, Guid.Parse(trimToGuid));
                                        await botClient.SendTextMessageAsync(callBack.Message.Chat.Id, "Введите новое время вебинара. В формате yyyy-MM-dd HH:mm", Telegram.Bot.Types.Enums.ParseMode.Default);
                                    }
                                    if (trimToEditObject.First() == 'p' && trimToGuid.Length == 36)
                                    {
                                        StateController.StateUpdate(callBack.From.Id, (int)AdminState.EditWebinarPrice, true, Guid.Parse(trimToGuid));
                                        await botClient.SendTextMessageAsync(callBack.Message.Chat.Id, "Введите новую цену вебинара", Telegram.Bot.Types.Enums.ParseMode.Default);
                                    }
                                    if (trimToEditObject.First() == 'o' && trimToGuid.Length == 36)
                                    {
                                        StateController.StateUpdate(callBack.From.Id, (int)AdminState.EditWebinarOnlineLink, true, Guid.Parse(trimToGuid));
                                        await botClient.SendTextMessageAsync(callBack.Message.Chat.Id, "Введите ссылку на онлайн трансляцию", Telegram.Bot.Types.Enums.ParseMode.Default);
                                    }
                                    if (trimToEditObject.First() == 's' && trimToGuid.Length == 36)
                                    {
                                        StateController.StateUpdate(callBack.From.Id, (int)AdminState.EditWebinarSavedLink, true, Guid.Parse(trimToGuid));
                                        await botClient.SendTextMessageAsync(callBack.Message.Chat.Id, "Введите ссылку на запись трансляции", Telegram.Bot.Types.Enums.ParseMode.Default);
                                    }
                                }
                                //else if (trimCallBack.First() == 'h')
                                //{
                                //    if (trimToEditObject.First() == 'd')//edit homework desc
                                //    {
                                //        StateController.StateUpdate(callBack.From.Id, (int)AdminState.EditHomeworkDescription, true, Guid.Parse(trimToGuid), false, false);
                                //        await botClient.SendTextMessageAsync(callBack.Message.Chat.Id, "Введите новое описание ДЗ", Telegram.Bot.Types.Enums.ParseMode.Default);
                                //    }
                                //}
                            }
                            else if (callBackSign == 'd')//delete
                            {
                                var trimToGuid = trimCallBack.Remove(0, 1);
                                if (trimCallBack.First() == 'c' && trimToGuid.Length == 36)//course 
                                {
                                    StateController.StateUpdate(callBack.From.Id, (int)AdminState.DeleteCourse, true, Guid.Parse(trimToGuid));
                                    await botClient.SendTextMessageAsync(callBack.Message.Chat.Id, "Что бы подтвердить удаление курса, введите 'Подтвердить'.", Telegram.Bot.Types.Enums.ParseMode.Default);
                                }
                                else if (trimCallBack.First() == 't' && trimToGuid.Length == 36)//tarif
                                {
                                    StateController.StateUpdate(callBack.From.Id, (int)AdminState.DeleteTarif, true, Guid.Parse(trimToGuid), true, false);
                                    await botClient.SendTextMessageAsync(callBack.Message.Chat.Id, "Что бы подтвердить удаление тарифа, введите 'Подтвердить'.", Telegram.Bot.Types.Enums.ParseMode.Default);
                                }
                                else if (trimCallBack.First() == 'l')//lesson
                                {
                                    var trimToDelObjectGuid = trimToGuid.Remove(0, 1);
                                    if (trimToGuid.First() == 'f' && trimToDelObjectGuid.Length == 36)
                                    {
                                        StateController.StateUpdate(callBack.From.Id, (int)AdminState.DeleteLessonFiles, true, Guid.Parse(trimToDelObjectGuid), false, true);
                                        await botClient.SendTextMessageAsync(callBack.Message.Chat.Id, "Что бы подтвердить удаление файлов, введите 'Подтвердить'.", Telegram.Bot.Types.Enums.ParseMode.Default);

                                    }
                                    else
                                    {
                                        StateController.StateUpdate(callBack.From.Id, (int)AdminState.DeleteLesson, true, Guid.Parse(trimToGuid), false, true);
                                        await botClient.SendTextMessageAsync(callBack.Message.Chat.Id, "Что бы подтвердить удаление урока, введите 'Подтвердить'.", Telegram.Bot.Types.Enums.ParseMode.Default);
                                    }
                                }
                                //else if (trimCallBack.First() == 'h')//HomeWork
                                //{
                                //    var trimToDelObjectGuid = trimToGuid.Remove(0, 1);
                                //    if (trimToGuid.First() == 'f')
                                //    {
                                //        StateController.StateUpdate(callBack.From.Id, (int)AdminState.DeleteHomeworkFiles, true, Guid.Parse(trimToDelObjectGuid), false, false);
                                //        await botClient.SendTextMessageAsync(callBack.Message.Chat.Id, "Что бы подтвердить удаление файлов, введите 'Подтвердить'.", Telegram.Bot.Types.Enums.ParseMode.Default);
                                //    }
                                //}
                            }
                            else if (callBackSign == 'a')//add lesson or tarif
                            {
                                var trimToGuid = trimCallBack.Remove(0, 1);
                                var trimToDelObjectGuid = trimToGuid.Remove(0, 1);
                                if (trimCallBack.First() == 'l')
                                {
                                    if (trimToGuid.First() == 'f' && trimToDelObjectGuid.Length == 36)
                                    {
                                        StateController.StateUpdate(callBack.From.Id, (int)AdminState.AddLessonsFiles, true, Guid.Parse(trimToDelObjectGuid), false, true);
                                        await botClient.SendTextMessageAsync(callBack.From.Id, "Добавте файлы урока, объёмом меньше 20 мб. Когда закончите, введите команду /end", Telegram.Bot.Types.Enums.ParseMode.Default);
                                    }
                                    else if (trimToGuid.First() == 'l' && trimToDelObjectGuid.Length == 36)
                                    {
                                        StateController.StateUpdate(callBack.From.Id, (int)AdminState.AddLessonsToEnd, true, Guid.Parse(trimToDelObjectGuid), true, false);//TODO
                                        await botClient.SendTextMessageAsync(callBack.From.Id, "Введите название урока", Telegram.Bot.Types.Enums.ParseMode.Default);
                                    }
                                    else if (trimToGuid.First() == 's' && trimToDelObjectGuid.Length == 36)
                                    {
                                        StateController.StateUpdate(callBack.From.Id, (int)AdminState.AddLessonsWithSeq, true, Guid.Parse(trimToDelObjectGuid), true, false);//TODO
                                        await botClient.SendTextMessageAsync(callBack.From.Id, "Введите номер урока", Telegram.Bot.Types.Enums.ParseMode.Default);
                                    }
                                    //else if (trimToGuid.First() == 'h')
                                    //{
                                    //    var lessons = db.Lessons.Where(x => x.Id == Guid.Parse(trimToDelObjectGuid)).Single();
                                    //    HomeWork homeWork = new HomeWork { Id = Guid.NewGuid(), Lesson = lessons };
                                    //    db.HomeWorks.Add(homeWork);
                                    //    db.SaveChanges();
                                    //    StateController.StateUpdate(callBack.From.Id, (int)AdminState.AddHomeworkDescription, true, homeWork.Id, false, false);
                                    //    await botClient.SendTextMessageAsync(callBack.From.Id, "Введите описание домашнего задания, к уроку '" + lessons.Name + "' под номером " + lessons.SeqNumber, Telegram.Bot.Types.Enums.ParseMode.Default);

                                    //}
                                    else if(trimToGuid.Length == 36)
                                    {
                                        var keyboard = new InlineKeyboardMarkup(new[]
                                            {
                                                new InlineKeyboardButton[]
                                                {
                                                    new InlineKeyboardButton{ Text = "Добавить урок в конец", CallbackData = "all" + Guid.Parse(trimToGuid) },
                                                    new InlineKeyboardButton{ Text = "Задать номер урока в ручную", CallbackData = "als" + Guid.Parse(trimToGuid) }
                                                },
                                                new InlineKeyboardButton[]
                                                {
                                                    new InlineKeyboardButton{ Text = "Назад", CallbackData = "Et" + Guid.Parse(trimToGuid) }
                                                }
                                            }
                                        );
                                        await botClient.SendTextMessageAsync(callBack.From.Id, "Создание нового урока.", Telegram.Bot.Types.Enums.ParseMode.Default, false, false, 0, keyboard);
                                    }
                                }
                                else if (trimCallBack.First() == 't')
                                {
                                    //TODO: ADD TARIF
                                    //StateController.StateUpdate(callBack.From.Id, (int)AdminState.AddTarifInExistinCourse, true, Guid.Parse(trimToGuid));//TODO
                                    var count = db.CoursesType.Where(x => x.IsDemo == true).ToList();
                                    if (count == null)
                                    {
                                        var keyboard = new InlineKeyboardMarkup(
                                            new InlineKeyboardButton[]
                                            {
                                                // First column
                                                new InlineKeyboardButton{ Text = "Да", CallbackData = "CreateDemoTrue"},
                                                new InlineKeyboardButton{ Text = "Нет", CallbackData = "CreateDemoFalse"}
                                            }
                                        );

                                        await botClient.SendTextMessageAsync(update.Message.Chat.Id, "Создать демо тариф?", Telegram.Bot.Types.Enums.ParseMode.Default, false, false, 0, keyboard);
                                    }
                                    else
                                    {
                                        //await botClient.AnswerCallbackQueryAsync(update.CallbackQuery.Id);
                                        CourseManager.AddCourseType(botClient, db, callBack.Message, Guid.Parse(trimToGuid), false);
                                    }
                                }
                                //else if (trimCallBack.First() == 'h')
                                //{
                                //    if (trimToGuid.First() == 'f')
                                //    {
                                //        StateController.StateUpdate(callBack.From.Id, (int)AdminState.AddHomeworkFiles, true, Guid.Parse(trimToDelObjectGuid), false, false);
                                //        await botClient.SendTextMessageAsync(callBack.From.Id, "Добавте файлы ДЗ, объёмом меньше 20 мб. Когда закончите, введите команду /end", Telegram.Bot.Types.Enums.ParseMode.Default);
                                //    }
                                //}

                            }
                            else if (callBackSign == 's' && trimCallBack.Length == 36)//change seq number
                            {
                                StateController.StateUpdate(callBack.From.Id, (int)AdminState.EditLessonSeqNumber, true, Guid.Parse(trimCallBack), false, true);
                                await botClient.SendTextMessageAsync(callBack.From.Id, "Введите новый номер урока!", Telegram.Bot.Types.Enums.ParseMode.Default);
                            }
                        }
                    }
                    else
                    {
                        if(StateController.GetLastBotMessageId(update.CallbackQuery.Message.Chat.Id)!= 0)
                        {
                            await botClient.DeleteMessageAsync(update.CallbackQuery.Message.Chat.Id, StateController.GetLastBotMessageId(update.CallbackQuery.Message.Chat.Id));

                        }
                        if (callBack.Data == "WebinarReg")
                        {
                            var lastWebinar = db.Webinars.OrderByDescending(x => x.WebinarDate).Take(1).SingleOrDefault();
                            ViewManager.WebinarRegister(botClient, db, update.CallbackQuery.Message, lastWebinar);
                        }
                        else if (callBack.Data == "MainMenu")
                        {
                            //await botClient.SendTextMessageAsync(update.CallbackQuery.Message.Chat.Id, "тест", replyToMessageId: update.CallbackQuery.Message.MessageId);
                            MessageController.MainMenu(user, db, update.CallbackQuery.Message, botClient);
                        }
                        else if (callBack.Data == "MyCoursesList")
                        {
                            ViewManager.MyCoursesList(botClient, db, update.CallbackQuery.Message);
                        }
                        else if (callBack.Data == "NewCoursesList")
                        {
                            ViewManager.NewCoursesList(botClient, db, update.CallbackQuery.Message);
                        }
                        else
                        {
                            var callBackSign = callBack.Data.First();
                            var trimCallBack = callBack.Data.Remove(0, 1);
                            var trimToGuid = callBack.Data.Remove(0, 2);
                            if (callBackSign == 'N')
                            {
                                if (trimCallBack.First() == 'C')
                                {
                                    ViewManager.SelectedNewCourse(botClient, db, update.CallbackQuery.Message, Guid.Parse(trimToGuid));
                                    //New course select
                                }
                                else if (trimCallBack.First() == 'L')
                                {
                                    //TODO: NExt lessons/
                                    StateController.UpdateProgressToFinish(Guid.Parse(trimToGuid), (int)update.CallbackQuery.Message.Chat.Id);
                                    var Lesson = db.Lessons.Where(x => x.Id == Guid.Parse(trimToGuid)).Include(x => x.CourseType).Include(x => x.CourseType.Course).Single();
                                    var lastLessonId = db.Lessons.Where(x => Lesson.CourseType == x.CourseType).OrderByDescending(x => x.SeqNumber).Select(x => x.Id).Take(1).Single();
                                    var NextLesson = db.Lessons.Where(x => x.CourseType == Lesson.CourseType && x.SeqNumber == Lesson.SeqNumber + 1).Single();
                                    if (NextLesson.IsPublished)
                                    {
                                        ViewManager.LessonView(botClient, db, update.CallbackQuery.Message, NextLesson.Id);
                                    }
                                }
                            }
                            else if (callBackSign == 'M')
                            {
                                if (trimCallBack.First() == 'C')
                                {
                                    //My course select
                                    ViewManager.MyCourseSelect(botClient, db, update.CallbackQuery.Message, Guid.Parse(trimToGuid));
                                }
                                else if (trimCallBack.First() == 'T')
                                {
                                    //My course type lessons list
                                    ViewManager.MyTarifSelect(botClient, db, update.CallbackQuery.Message, Guid.Parse(trimToGuid));
                                }
                                else if (trimCallBack.First() == 'N')
                                {
                                    //Add open tarif to learn/
                                    ViewManager.MyNewTarifStart(botClient, db, update.CallbackQuery.Message, Guid.Parse(trimToGuid));
                                }
                                else if (trimCallBack.First() == 'L')
                                {
                                    //TODO: Add open tarif to learn/
                                    var progress = db.Progresses.Where(x => x.ProgressObjectId == Guid.Parse(trimToGuid) && x.User == update.CallbackQuery.Message.Chat.Id).SingleOrDefault();
                                    if (progress == null)
                                        StateController.AddProgress(Guid.Parse(trimToGuid), (int)ProgressObject.Lesson, (int)update.CallbackQuery.Message.Chat.Id);
                                    ViewManager.LessonView(botClient, db, update.CallbackQuery.Message, Guid.Parse(trimToGuid));
                                }
                            }
                            else if (callBackSign == 'S')
                            {
                                if (trimCallBack.First() == 'C')
                                {
                                    ViewManager.StartNewCourse(botClient, db, update.CallbackQuery.Message, Guid.Parse(trimToGuid));
                                    //New course start
                                }
                            }
                            else if (callBackSign == 'P')
                            {
                                if (trimCallBack.First() == 'R')
                                {
                                    Message mess;
                                    Payment PaymentTest;
                                    if (trimToGuid.First() == 'W')
                                    {
                                        
                                        PaymentTest = db.Payments.Where(x => x.UserId == (int)update.CallbackQuery.Message.Chat.Id && x.WebinarId == Guid.Parse(trimToGuid.Remove(0, 1))).SingleOrDefault();

                                        if (PaymentTest != null)
                                        {
                                            var keyboardInline = new InlineKeyboardButton[1][];
                                            keyboardInline[0] = new[]
                                                {
                                                    new InlineKeyboardButton { Text = "Назад", CallbackData = "MainMenu"},
                                                };
                                            var keyboard = new InlineKeyboardMarkup(keyboardInline);
                                            mess = await botClient.SendTextMessageAsync(update.CallbackQuery.Message.Chat.Id, "Ваш запрос на обработке!", Telegram.Bot.Types.Enums.ParseMode.Default, false, false, 0, keyboard);

                                            StateController.SetLastBotMessageId(update.CallbackQuery.Message.Chat.Id, mess.MessageId);
                                        }
                                        else
                                        {
                                            var Webinar = db.Webinars.Where(x => x.Id == Guid.Parse(trimToGuid.Remove(0, 1))).Single();
                                            var LastPayment = db.Payments.OrderByDescending(x => x.UnicId).Take(1).SingleOrDefault();
                                            int UnicId;
                                            if (LastPayment == null)
                                            {
                                                UnicId = 1;
                                            }
                                            else
                                            {
                                                UnicId = LastPayment.UnicId + 1;
                                            }
                                            var Payment = new Payment()
                                            {
                                                WebinarId = Webinar.Id,
                                                Amount = Webinar.Amount,
                                                Id = Guid.NewGuid(),
                                                IsOffline = true,
                                                Status = false,
                                                UserId = (int)update.CallbackQuery.Message.Chat.Id,
                                                UnicId = UnicId
                                            };//???
                                            db.Payments.Add(Payment);
                                            db.SaveChanges();
                                            var keyboard = new InlineKeyboardMarkup(new[]
                                                {
                                                new InlineKeyboardButton { Text = "Назад", CallbackData = "MainMenu"} //TODO: 
                                                }
                                            );
                                            await botClient.SendTextMessageAsync(update.CallbackQuery.Message.Chat.Id, "Напишите @wearefsociety для завершения оплаты «хочу оплатить вебинар».", Telegram.Bot.Types.Enums.ParseMode.Default, false, false, 0, keyboard);
                                            
                                        }
                                    }
                                    else
                                    {
                                        PaymentTest = db.Payments.Where(x => x.UserId == (int)update.CallbackQuery.Message.Chat.Id && x.CourseTypeId == Guid.Parse(trimToGuid)).SingleOrDefault();
                                        if (PaymentTest != null)
                                        {
                                            mess = await botClient.SendTextMessageAsync(update.CallbackQuery.Message.Chat.Id, "Ваш запрос на обработке!");
                                        }
                                        else
                                        {
                                            var CourseType = db.CoursesType.Where(x => x.Id == Guid.Parse(trimToGuid)).Include(x => x.Course).Single();
                                            var LastPayment = db.Payments.OrderByDescending(x => x.UnicId).Take(1).SingleOrDefault();
                                            int UnicId;
                                            if (LastPayment == null)
                                            {
                                                UnicId = 1;
                                            }
                                            else
                                            {
                                                UnicId = LastPayment.UnicId + 1;
                                            }
                                            var Payment = new Payment()
                                            {
                                                CourseTypeId = CourseType.Id,
                                                Amount = CourseType.PaymentAmount,
                                                Id = Guid.NewGuid(),
                                                IsOffline = true,
                                                Status = false,
                                                UserId = (int)update.CallbackQuery.Message.Chat.Id,
                                                UnicId = UnicId
                                            };//???
                                            db.Payments.Add(Payment);
                                            db.SaveChanges();
                                            var keyboard = new InlineKeyboardMarkup(new[]
                                                {
                                                new InlineKeyboardButton { Text = "Назад", CallbackData = "MC" + CourseType.Course.Id}
                                            });
                                            mess = await botClient.SendTextMessageAsync(update.CallbackQuery.Message.Chat.Id, "Напиши админу @wearefsociety, для завершения оплаты!", Telegram.Bot.Types.Enums.ParseMode.Default, false, false, 0, keyboard);

                                        }
                                        StateController.SetLastBotMessageId(update.CallbackQuery.Message.Chat.Id, mess.MessageId);
                                    }
                                    //Payment request
                                }
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}