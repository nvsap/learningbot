﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using telegramLearnBot.Controllers.AdminActions;
using telegramLearnBot.Models;
using telegramLearnBot.Models.LearningModels;

namespace telegramLearnBot.Controllers
{
    public class StateController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public static void AddProgress(Guid ObjectId, int TypeObject, int userId)
        {
            try
            {
                using (TelegramContext db = new TelegramContext())
                {
                    Progress progress = new Progress() { Id = Guid.NewGuid(), IsFinished = false, ProgressObjectId = ObjectId, ProgressObjectType = TypeObject, SartDate = DateTime.Now, User = userId };

                    db.Progresses.Add(progress);
                    db.SaveChanges();
                }
            }
            catch(Exception ex)
            {
                throw;
            }
        }
        public static void UpdateProgressToFinish(Guid ObjectId, int userId)
        {
            try
            {
                using (TelegramContext db = new TelegramContext())
                {
                    var progress = db.Progresses.Where(x => x.ProgressObjectId == ObjectId && x.User == userId).SingleOrDefault();
                    progress.IsFinished = true;
                    db.Progresses.Update(progress);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public static void StateUpdate(long userId, int state, bool isAdmin)
        {
            using (TelegramContext db = new TelegramContext())
            {
                var userState = db.UserStates.Where(x => x.User.UserId == userId).ToList();
                userState[0].State = state;
                db.UserStates.Update(userState[0]);
                db.SaveChanges();
            }
        }
        public static void StateUpdate(long userId, int state, bool isAdmin, Guid CourseId)
        {
            using (TelegramContext db = new TelegramContext())
            {
                var userState = db.UserStates.Where(x => x.User.UserId == userId).ToList();
                userState[0].State = state;
                userState[0].CourseAtWorkId = CourseId;
                db.UserStates.Update(userState[0]);
                db.SaveChanges();
            }
        }
        public static void StateUpdate(long userId, int state, bool isAdmin, Guid SomeId, bool isCourseType, bool isLesson, bool isNewTarif = false)
        {
            using (TelegramContext db = new TelegramContext())
            {
                var userState = db.UserStates.Where(x => x.User.UserId == userId).ToList();
                userState[0].State = state;
                if (isCourseType)
                {
                    userState[0].CourseTypeAtWorkId = SomeId;
                }
                else if (isLesson)
                {
                    userState[0].LessonAtWorkId = SomeId;            
                }
                else
                {
                    userState[0].HomeWorkAtWorkId = SomeId;
                }

                if (isNewTarif)
                {
                    userState[0].LessonsSeqNumber = 1;
                }
                db.UserStates.Update(userState[0]);
                db.SaveChanges();
            }
        }
        public static void SetLastUserMessageId(long userId, int messageId)
        {
            using (TelegramContext db = new TelegramContext())
            {
                var userState = db.UserStates.Where(x => x.User.UserId == userId).ToList();
                userState[0].UserMessageId = messageId;
                db.UserStates.Update(userState[0]);
                db.SaveChanges();
            }
        }
        public static int GetLastUserMessageId(long userId)
        {
            using (TelegramContext db = new TelegramContext())
            {
                var userState = db.UserStates.Where(x => x.User.UserId == userId).ToList();
                return userState[0].UserMessageId;
            }
        }
        public static void SetLastBotMessageId(long userId, int messageId)
        {
            using (TelegramContext db = new TelegramContext())
            {
                var userState = db.UserStates.Where(x => x.User.UserId == userId).ToList();
                userState[0].BotMessageId = messageId;
                db.UserStates.Update(userState[0]);
                db.SaveChanges();
            }
        }
        public static int GetLastBotMessageId(long userId)
        {
            using (TelegramContext db = new TelegramContext())
            {
                var userState = db.UserStates.Where(x => x.User.UserId == userId).SingleOrDefault();
                if(userState == null)
                {
                    return 0;
                }
                return userState.BotMessageId;
            }
        }
        public static void StateControl(Telegram.Bot.TelegramBotClient botClient, Update update)
        {
            using (TelegramContext db = new TelegramContext())
            {
                var userState = db.UserStates.Where(x => x.User.UserId == update.Message.From.Id).ToList();
                ActionStateSelected(userState[0], db, botClient, update);
            }
        }

        public static void ActionStateSelected(Models.LearningModels.UserStates userStates, 
                                                    TelegramContext db,
                                                    Telegram.Bot.TelegramBotClient botClient,
                                                    Update update)
        {
            switch (userStates.State)
            {
                case 0://CallBackMainMenu
                    break;
                case 1://AddCourse
                    break;
                case 2://CoursesList
                    break;
                case 3://AddCourseName
                    CourseManager.AddCoursesName(botClient, db, update.Message);
                    break;
                case 4://AddCourseDescription
                    CourseManager.AddCourseDescription(botClient, db, update.Message, userStates.CourseAtWorkId);
                    break;
                case 5://AddTarifName
                    CourseManager.AddCourseTypeName(botClient, db, update.Message, userStates.CourseAtWorkId, userStates.CourseTypeAtWorkId);
                    break;
                case 6://AddTarifDescription
                    CourseManager.AddCourseTypeDescription(botClient, db, update.Message, userStates.CourseAtWorkId, userStates.CourseTypeAtWorkId);
                    break;
                case 7://AddTarifAmount
                    CourseManager.AddCourseTypePrice(botClient, db, update.Message, userStates.CourseAtWorkId, userStates.CourseTypeAtWorkId);
                    break;
                case 8://AddLessons
                    CourseManager.AddLessonName(botClient, db, update.Message, userStates.CourseAtWorkId, userStates.CourseTypeAtWorkId);
                    break;
                case 9://AddLessonsDescription
                    CourseManager.AddLessonDescription(botClient, db, update.Message, userStates.CourseAtWorkId, userStates.CourseTypeAtWorkId, userStates.LessonAtWorkId);
                    break;
                case 10://AddLessonsFiles
                    FilesManager.SaveFile(botClient, update.Message, userStates.LessonAtWorkId, (int)ParentData.Lessons);
                    break;
                case 11://AddHomeworkDescription
                    //CourseManager.AddHomeworkDescription(botClient, db, update.Message, userStates.HomeWorkAtWorkId);
                    break;
                case 12://AddHomeWorkFile
                    //FilesManager.SaveFile(botClient, update.Message, userStates.HomeWorkAtWorkId, (int)ParentData.HomeWorkTask);
                    break;
                case 13:
                    EditManager.EditCourseName(botClient, db, update.Message, userStates.CourseAtWorkId);
                    break;
                case 14:
                    EditManager.EditCourseDescription(botClient, db, update.Message, userStates.CourseAtWorkId);
                    break;
                case 15:
                    EditManager.EditTarifName(botClient, db, update.Message, userStates.CourseTypeAtWorkId);
                    break;
                case 16:
                    EditManager.EditTarifDescription(botClient, db, update.Message, userStates.CourseTypeAtWorkId);
                    break;
                case 17:
                    EditManager.EditTarifPrice(botClient, db, update.Message, userStates.CourseTypeAtWorkId);
                    break;
                case 18:
                    EditManager.EditLessonName(botClient, db, update.Message, userStates.LessonAtWorkId);
                    break;
                case 19:
                    EditManager.EditLessonDesc(botClient, db, update.Message, userStates.LessonAtWorkId);
                    break;
                case 20:
                    //EditManager.EditHomeWorkDesc(botClient, db, update.Message, userStates.HomeWorkAtWorkId);
                    break;
                case 21:
                    if(update.Message.Text == "Подтвердить")
                    {
                        EditManager.DeleteLesson(botClient, db, update.Message, userStates.LessonAtWorkId);
                    }
                    break;
                case 22:
                    if (update.Message.Text == "Подтвердить")
                    {
                        EditManager.DeleteTarif(botClient, db, update.Message, userStates.CourseTypeAtWorkId);
                    }
                    break;
                case 23:
                    if (update.Message.Text == "Подтвердить")
                    {
                        EditManager.DeleteCourse(botClient, db, update.Message, userStates.CourseAtWorkId);
                    }
                    break;
                case 24://LEssons files del
                    if (update.Message.Text == "Подтвердить")
                    {
                        db.DataFiles.RemoveRange(db.DataFiles.Where(x => x.ParentId == userStates.LessonAtWorkId));
                        db.SaveChanges();
                        botClient.SendTextMessageAsync(update.Message.Chat.Id, "Файлы урока удалены.");
                    }
                    break;
                case 25://Homework files dell
                    if (update.Message.Text == "Подтвердить")
                    {
                        db.DataFiles.RemoveRange(db.DataFiles.Where(x => x.ParentId == userStates.HomeWorkAtWorkId));
                        db.SaveChanges();
                        botClient.SendTextMessageAsync(update.Message.Chat.Id, "Файлы ДЗ удалены.");
                    }
                    break;
                case 26://Change Seq Number
                    EditManager.EditLessonSeqNumber(botClient, db, update.Message, userStates.LessonAtWorkId);
                    break;
                case 27://Add Lesson To End
                    CourseManager.AddLessonName(botClient, db, update.Message, userStates.CourseTypeAtWorkId);
                    break;
                case 28://Add Lesson With Seq
                    try
                    {
                        int seq = Convert.ToInt32(update.Message.Text);
                        CourseManager.AddLesson(botClient, db, update.Message, userStates.CourseTypeAtWorkId, seq);
                    }
                    catch (Exception ex)
                    {
                        botClient.SendTextMessageAsync(update.Message.Chat.Id, "Ошибка создания урока. " + ex.Message);
                    }
                    break;
                case 29://Add Lesson To End
                    CourseManager.AddLessonName(botClient, db, userStates.LessonAtWorkId, update.Message);
                    break;
                case 30: //PaymentsSearch
                    ViewManager.PaymentsSearch(botClient, db, update.Message);
                    break;
                case 31: //AddWebinarName
                    WebinarsManager.AddWebinarName(botClient, db, update.Message);
                    break;
                case 32: //AddWebinarDescription
                    WebinarsManager.AddWebinarDescription(botClient, db, update.Message, userStates.CourseAtWorkId);
                    break;
                case 33: //AddWebinarDate
                    WebinarsManager.AddWebinarDate(botClient, db, update.Message, userStates.CourseAtWorkId);
                    break;
                case 34: //AddWebinarPrice
                    WebinarsManager.AddWebinarPrice(botClient, db, update.Message, userStates.CourseAtWorkId);
                    break;
                case 37: //EditWebinarName
                    WebinarsManager.WebinarsEditName(botClient, db, update.Message, userStates.CourseAtWorkId);
                    break;
                case 38: //EditWebinarDescription
                    WebinarsManager.WebinarsEditDescr(botClient, db, update.Message, userStates.CourseAtWorkId);
                    break;
                case 40: //EditWebinarDate
                    WebinarsManager.WebinarsEditPrice(botClient, db, update.Message, userStates.CourseAtWorkId);
                    break;
                case 39: //EditWebinarPrice
                    WebinarsManager.WebinarsListDate(botClient, db, update.Message, userStates.CourseAtWorkId);
                    break;
                case 41: //EditWebinarOnlineLink
                    WebinarsManager.WebinarsListOnlineLink(botClient, db, update.Message, userStates.CourseAtWorkId);
                    break;
                case 42: //EditWebinarSavedLink
                    WebinarsManager.WebinarsListSavedLink(botClient, db, update.Message, userStates.CourseAtWorkId);
                    break;
                case 43: //EditWebinarSavedLink
                    CourseManager.AddLessonTime(botClient, db, update.Message, userStates.LessonAtWorkId);
                    break;
                case 44: //EditWebinarSavedLink
                    EditManager.EditPostTime(botClient, db, update.Message, userStates.LessonAtWorkId);
                    break;

            }
        }
    }
}