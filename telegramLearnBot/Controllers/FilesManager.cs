﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.InputFiles;
using telegramLearnBot.Models;
using telegramLearnBot.Models.LearningModels;

namespace telegramLearnBot.Controllers
{
    public class FilesManager
    {
        public static async void SaveFile(TelegramBotClient botClient, Message message, Guid parentGuid, int EnumDataFileParent)
        {
            if (message.Document != null)
            {
                using (TelegramContext db = new TelegramContext())
                {
                    DataFile LessonsData = new DataFile { FileId = message.Document.FileId, Id = Guid.NewGuid(), Name = message.Document.FileName, ParentType = EnumDataFileParent, ParentId = parentGuid };
                    await db.DataFiles.AddAsync(LessonsData);
                    await db.SaveChangesAsync();
                }
            }
            else
            {
                await botClient.SendTextMessageAsync(message.Chat.Id, "Отправте следующий файл, или введите одну из команд!", Telegram.Bot.Types.Enums.ParseMode.Default);
            }
        }
        public static async void SendFile(TelegramBotClient botClient, Message message, string FileId)
        {
                //var file = await botClient.GetFileAsync(FileId);
                await botClient.SendDocumentAsync(message.Chat.Id, FileId);
                //await botClient.SendDocumentAsync(message.Chat.Id, file);
            
        }
    }
}
