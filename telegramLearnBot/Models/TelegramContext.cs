﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using telegramLearnBot.Models.LearningModels;

namespace telegramLearnBot.Models
{
    public class TelegramContext : DbContext
    {
        

        public DbSet<User> Users { get; set; }

        public DbSet<Course> Courses { get; set; }

        public DbSet<CourseType> CoursesType { get; set; }

        public DbSet<Lesson> Lessons { get; set; }

        public DbSet<Progress> Progresses { get; set; }

        public DbSet<Payment> Payments { get; set; }

        public DbSet<DataFile> DataFiles { get; set; }

        //public DbSet<HomeWork> HomeWorks { get; set; }

        public DbSet<HomeWorkRequest> HomeWorkRequests { get; set; }

        public DbSet<PotentialUsers> PotentialUsers { get; set; }

        public DbSet<UserStates> UserStates { get; set; }
        public DbSet<Webinars> Webinars { get; set; }
        public DbSet<WebinarsUsers> WebinarsUsers { get; set; }

        public TelegramContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=tcp:93.190.46.34,33301; Initial Catalog = uh1141519_db; User Id = uh1141519_user; Password = x1rQkvnk96;");
        }
    }
}
