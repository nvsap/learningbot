﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace telegramLearnBot.Models
{
    public class PaymentModels
    {
        public int MERCHANT_ID { get; set; }
        public decimal AMOUNT { get; set; }
        public int intId { get; set; }
        public int MERCHANT_ORDER_ID { get; set; }
        public string P_EMAIL { get; set; }
        public string P_PHONE { get; set; }
        public int CUR_ID { get; set; }
        public string SIGN { get; set; }

    }
}
