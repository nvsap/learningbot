﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace telegramLearnBot.Models.LearningModels
{
    public class User
    {
        public Guid Id { get; set; }

        public int UserId { get; set; }

        public string Name { get; set; }

        public bool IsAdmin { get; set; }
    }

    public class Course
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
        public bool IsHiden { get; set; }

    }

    public class CourseType
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public decimal PaymentAmount { get; set; }

        public virtual Course Course { get; set; }

        public bool IsUseMentor { get; set; }

        public bool IsDemo { get; set; }
    }


    public class Lesson
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public int SeqNumber { get; set; }

        public string Desciption { get; set; }

        public virtual CourseType CourseType { get; set; }
        public DateTime PostTime { get; set; }
        public bool IsPublished { get; set; }
        public bool HasHomework { get; set; }
    }

    //public class HomeWork
    //{
    //    public Guid Id { get; set; }

    //    public Lesson Lesson { get; set; }

    //    public string Description { get; set; }
    //}

    public class HomeWorkRequest
    {
        public Guid Id { get; set; }

        public virtual Lesson Lesson { get; set; }

        public virtual User User { get; set; }

        public string Description { get; set; }

        public bool IsFeedBack { get; set; }

        public bool IsRequest { get; set; }

        public bool IsRequestSuccessful { get; set; }
    }


    public class DataFile
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string FileId { get; set; }
        public int ParentType { get; set; }
        public Guid ParentId { get; set; }
    }


    public class Progress
    {
        public Guid Id { get; set; }

        public Guid ProgressObjectId { get; set; }
        public int ProgressObjectType { get; set; }

        //public HomeWorkRequest HomeWorkRequest { get; set; }

        public bool IsFinished { get; set; }

        //public bool IsHomeWork { get; set; }

        public int User { get; set; }

        public DateTime SartDate { get; set; }

        public DateTime EndDate { get; set; }
    }

    public class Payment
    {
        public Guid Id { get; set; }

        public int UserId { get; set; }

        public Guid? CourseTypeId { get; set; }
        public Guid? WebinarId { get; set; }

        public bool Status { get; set; }

        public bool IsOffline { get; set; }

        public decimal Amount { get; set; }

        public DateTime DateTime { get; set; } = DateTime.Now;
        public int UnicId { get; set; }
    }

    public class PotentialUsers
    {
        public Guid Id { get; set; }

        public virtual User User { get; set; }

        public virtual Course Course { get; set; }

        bool IsGoingToPayments { get; set; }

        DateTime DateTime { get; set; }
    }
    public class WebinarsUsers
    {
        public Guid Id { get; set; }
        public virtual User User { get; set; }
        public virtual Webinars Webinars { get; set; }
        public DateTime RegTime { get; set; }

        public WebinarsUsers()
        {
            Id = Guid.NewGuid();
            RegTime = DateTime.UtcNow;
        }
    }
    public class Webinars
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime WebinarDate { get; set; }
        public string OnlineLink { get; set; }
        public string SavedTranslationLink { get; set; }
        public decimal Amount { get; set; } 
        public Webinars()
        {
            Id = Guid.NewGuid();
        }
    }
    public class UserStates
    {
        public Guid Id { get; set; }
        public virtual User User { get; set; }
        public int State { get; set; }
        public Guid CourseAtWorkId { get; set; }
        public Guid CourseTypeAtWorkId { get; set; }
        public Guid LessonAtWorkId { get; set; }
        public Guid HomeWorkAtWorkId { get; set; }
        public int LessonsSeqNumber { get; set; } = 1;
        public int UserMessageId { get; set; }
        public int BotMessageId { get; set; }
    }

    public enum ParentData
    {
        Lessons,
        //HomeWorkTask,
        HomeWorkRequest,
        HomeWorkFeedBack
    }

    public enum ProgressObject
    {
        Course,
        Tarif,
        Lesson
    }
    public enum DustState
    {

    }

    public enum AdminState
    {
        CallBackMainMenu, 
        AddCourse,
        CoursesList,
        AddCourseName,
        AddCourseDescription,
        AddTarifName,
        AddTarifDescription,
        AddTarifAmount,
        AddLessons,
        AddLessonsDescription,
        AddLessonsFiles,
        AddHomeworkDescription,
        AddHomeworkFiles,
        EditCourseName,
        EditCourseDescription,
        EditTarifName,
        EditTarifDescription,
        EditTarifPrice,
        EditLessonName,
        EditLessonDescription,
        EditHomeworkDescription,
        DeleteLesson,
        DeleteTarif,
        DeleteCourse,
        DeleteLessonFiles,
        DeleteHomeworkFiles,
        EditLessonSeqNumber,
        AddLessonsToEnd,
        AddLessonsWithSeq,
        AddLessonsName,
        PaymentSearch,
        AddWebinarName,
        AddWebinarDescription,
        AddWebinarDate,
        AddWebinarPrice,
        AddWebinarOnlineLink,
        AddWebinarSavedLink,
        EditWebinarName,
        EditWebinarDescription,
        EditWebinarDate,
        EditWebinarPrice,
        EditWebinarOnlineLink,
        EditWebinarSavedLink,
        AddLessonsTime,
        EditLessonsTime
    }
}
