﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using telegramLearnBot.Models;

namespace telegramLearnBot.Models.Commands
{
    public class StartCommand : Command
    {
        public override string Name => @"/start";

        public override bool Contains(Message message)
        {
            if (message.Type != Telegram.Bot.Types.Enums.MessageType.Text)
                return false;

            return message.Text.Contains(this.Name);
        }

        public override async Task Execute(Message message, TelegramBotClient botClient)
        {
            var chatId = message.Chat.Id;
            using(TelegramContext db = new TelegramContext())
            {
                var Users = db.Users.Where(p => p.UserId == message.From.Id).ToList();
                if (Users.Count() == 0)
                {
                    LearningModels.User user = new LearningModels.User { UserId = message.From.Id, Name = message.From.Username, IsAdmin = false };
                    LearningModels.UserStates US = new LearningModels.UserStates { User = user, Id = Guid.NewGuid() };
                    db.Users.Add(user);
                    db.UserStates.Add(US);
                    await db.SaveChangesAsync();
                    Console.WriteLine("Saved new user " + user.Name);
                    Controllers.MessageController.MainMenu(user, db, message, botClient);
                }
                else
                    Controllers.MessageController.MainMenu(Users[0], db, message, botClient);
            }
        }

    }
}
