﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using telegramLearnBot.Models.LearningModels;
using Microsoft.EntityFrameworkCore;

namespace telegramLearnBot.Models.Commands
{
    public class NextLessonsCommand : Command
    {
        public override string Name => @"/nextL";

        public override bool Contains(Message message)
        {
            if (message.Type != Telegram.Bot.Types.Enums.MessageType.Text)
                return false;

            return message.Text.Contains(this.Name);
        }

        public override async Task Execute(Message message, TelegramBotClient botClient)
        {
            var chatId = message.Chat.Id;
            using (TelegramContext db = new TelegramContext())
            {
                var userState = db.UserStates.Where(p => p.User.UserId == message.From.Id).Include(x => x.User).Single();
                if (userState.User.IsAdmin)
                {
                    //var lessons = db.Lessons.Where(x => x.Id == userState.LessonAtWorkId).Single();
                    //if (lessons.HasHomework && userState.State == (int)AdminState.AddLessonsFiles)
                    //{
                    //    HomeWork homeWork = new HomeWork { Id = Guid.NewGuid(), Lesson = lessons };
                    //    userState.State = (int)AdminState.AddHomeworkDescription;
                    //    userState.HomeWorkAtWorkId = homeWork.Id;
                    //    db.HomeWorks.Add(homeWork);
                    //    db.UserStates.Update(userState);
                    //    db.SaveChanges();
                    //    await botClient.SendTextMessageAsync(message.Chat.Id, "Введите описание домашнего задания, к уроку '" + lessons.Name + "' под номером " +lessons.SeqNumber, Telegram.Bot.Types.Enums.ParseMode.Default);

                    //}
                    //else if (userState.State == (int)AdminState.AddHomeworkFiles || userState.State == (int)AdminState.AddLessonsFiles)
                    //{
                        userState.State = (int)AdminState.AddLessons;
                        db.UserStates.Update(userState);
                        db.SaveChanges();
                        await botClient.SendTextMessageAsync(message.Chat.Id, "Введите название урока", Telegram.Bot.Types.Enums.ParseMode.Default);
                    //}
                }
            }
        }
    }
}
