﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using telegramLearnBot.Controllers.AdminActions;
using telegramLearnBot.Models.LearningModels;

namespace telegramLearnBot.Models.Commands
{
    public class NextTarifCommand : Command
    {
        public override string Name => @"/nextT";

        public override bool Contains(Message message)
        {
            if (message.Type != Telegram.Bot.Types.Enums.MessageType.Text)
                return false;

            return message.Text.Contains(this.Name);
        }

        public override async Task Execute(Message message, TelegramBotClient botClient)
        {
            using (TelegramContext db = new TelegramContext())
            {
                var userState = db.UserStates.Include(x => x.User).Where(p => p.User.UserId == message.From.Id).Single();
                if ((userState.State == (int)AdminState.AddHomeworkFiles || userState.State == (int)AdminState.AddLessonsFiles) && userState.User.IsAdmin)
                {
                    CourseManager.AddCourseType(botClient, db, message, userState.CourseAtWorkId);
                }
            }
        }
    }
}
