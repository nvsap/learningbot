﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using telegramLearnBot.Models.LearningModels;
using Microsoft.EntityFrameworkCore;

namespace telegramLearnBot.Models.Commands
{
    public class EndCommand : Command
    {
        public override string Name => @"/end";

        public override bool Contains(Message message)
        {
            if (message.Type != Telegram.Bot.Types.Enums.MessageType.Text)
                return false;

            return message.Text.Contains(this.Name);
        }

        public override async Task Execute(Message message, TelegramBotClient botClient)
        {
            using (TelegramContext db = new TelegramContext())
            {
                var userState = db.UserStates.Where(p => p.User.UserId == message.From.Id).Include(x => x.User).Single();
                userState.LessonAtWorkId = Guid.Empty;
                userState.LessonsSeqNumber = 0;
                userState.HomeWorkAtWorkId = Guid.Empty;
                userState.CourseAtWorkId = Guid.Empty;
                userState.CourseTypeAtWorkId = Guid.Empty;
                db.UserStates.Update(userState);
                await db.SaveChangesAsync();
            }
        }
    }
}
