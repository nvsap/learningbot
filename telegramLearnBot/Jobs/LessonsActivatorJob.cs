﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using telegramLearnBot.Controllers;
using telegramLearnBot.Models;

namespace telegramLearnBot.Jobs
{
    public class LessonsActivatorJob : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            using (TelegramContext db = new TelegramContext())
            {
                var botClient = await Bot.GetBotClientAsync();
                Message mess;

                var LessonsList = db.Lessons.Where(x => DateTime.Now >= x.PostTime  && !x.IsPublished).ToList();
                LessonsList.ForEach(x => x.IsPublished = true);
                db.Lessons.UpdateRange(LessonsList);
                var ClientList = from pay in db.Payments
                                 join les in LessonsList on pay.CourseTypeId equals les.CourseType.Id  
                                 where pay.Status == true
                                 select new { pay.UserId, les};
                await db.SaveChangesAsync();
                if (ClientList != null && ClientList.Count() > 0)
                {
                    foreach (var item in ClientList)
                    {
                        var b = StateController.GetLastBotMessageId(item.UserId);
                        if (b != 0)
                        {
                            await botClient.DeleteMessageAsync(item.UserId, b);
                        }
                        var keyboardInline = new InlineKeyboardButton[2][];
                        keyboardInline[0] = new[]
                        {
                        new InlineKeyboardButton { Text = item.les.Name, CallbackData = "ML" + item.les.Id }
                    };
                        keyboardInline[1] = new[]
                                {
                            new InlineKeyboardButton { Text = "Назад", CallbackData = "MainMenu"}
                        };
                        var keyboard = new InlineKeyboardMarkup(keyboardInline);
                        mess = await botClient.SendTextMessageAsync(item.UserId, "Доступ к '" + item.les.Name + "' открыт!", Telegram.Bot.Types.Enums.ParseMode.Markdown, false, false, 0, keyboard);
                        StateController.SetLastBotMessageId(item.UserId, mess.MessageId);
                    }
                }
                
                
            }
        }
    }
    public class Scheduler
    {
        public static async void Start()
        {
            IScheduler scheduler = await StdSchedulerFactory.GetDefaultScheduler();
            await scheduler.Start();

            IJobDetail job = JobBuilder.Create<LessonsActivatorJob>().Build();

            ITrigger trigger = TriggerBuilder.Create()  // создаем триггер
                .WithIdentity("LessonsActivate", "Lessons")     // идентифицируем триггер с именем и группой
                .StartNow()                            // запуск сразу после начала выполнения
                .WithSimpleSchedule(x => x            // настраиваем выполнение действия
                    .WithIntervalInMinutes(1)          // через 1 минуту
                    .RepeatForever())                   // бесконечное повторение
                .Build();                               // создаем триггер

            await scheduler.ScheduleJob(job, trigger);        // начинаем выполнение работы
        }
    }
}
